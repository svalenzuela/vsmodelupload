/*
 * Copyright 2013, Raptorcode Studios Inc, 
 * Author, Kaj Dijkstra.
 * All rights reserved.
 *
 */
 

/**
 * Scene Node
**/
raptorjs.sceneNode = function(){
	this._className = 'sceneNode';
	this.rootEntity;
	this.entitys = [];
}


/**
 * add entity to sceneNode
 * @param {(entity)} entity
**/
raptorjs.sceneNode.prototype.addEntity = function(entity){
	this.entitys.push(entity);
	//if(this.rootEntity)
	//this.rootEntity.addChild(entity);

}


/**
 * get entity's
 * @param {(entity)} entity
**/
raptorjs.sceneNode.prototype.getEntitys = function(entity){
	return this.entitys;
}

raptorjs.sceneNode.prototype.getEntityByName = function(name){
	for(var c = 0; c<this.entitys.length;c++) {
		if(this.entitys[c].name == name)
			return this.entitys[c];
	}
}

raptorjs.sceneNode.prototype.removeEntityByName = function(name){
	for(var c = 0; c<this.entitys.length;c++) {
		if(this.entitys[c].name == name)
			this.entitys.splice(c, 1);
	}
	
	
	var children = raptorjs.system.scene.rootEntity.children[0].children;
	for(var c = 0; c<children.length;c++) {
		if(children[c].name == name)
			children.splice(c, 1);
	}
	
}