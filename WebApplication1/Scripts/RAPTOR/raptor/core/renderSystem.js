/*
 * Copyright 2012, Raptorcode Studios Inc, 
 * Author, Kaj Dijkstra.
 * All rights reserved.
 *
 */
 

/**
 * Rendersystem
**/
raptorjs.renderSystem = function( ) {
	this.scene = raptorjs.createObject("sceneNode");
	
	this.quadView;
	this.quadProjection;
	this.quadViewProjection;
	
	this.depthShader;
	this.shadowShader;
	this.shadowCameras = [];
	
	this.deferredShader;
	this.deferredFramebuffer;

	this.fxaaShader;
	this.fxaaFramebuffer;
	
	this.ambientOcclusionFramebuffer;
	this.ambientocclusionShader;
	
	this.ambientOcclusionXFramebuffer;
	this.ambientOcclusionFinalFramebuffer;
	
	this.infoSampler;
	
	var quadVertices;
	var quadIndices;
	var quadUv;
	
	this.reflectionSampler;
	this.cornerVertices = [ [-1,1,-1],[1,1,-1],  [1,-1,-1],[-1,-1,-1],  //near
						[-1,1,1], [1,1,1],   [1,-1,1], [-1,-1,1] ];	
	
	this.deferred = true;
	
	this.quad = {};
}

raptorjs.renderSystem.prototype.render = function( ) {
	//this.renderShadowDepth();
	
	if(this.deferred) {
		this.renderScene();
		this.renderAmbientOcclusion();
		
		
		gl.bindFramebuffer(gl.FRAMEBUFFER, this.ambientOcclusionXFramebuffer.glFramebuffer );
		
		this.ssaoConvolutionShaderX.update();

		gl.clearColor( 1, 1, 1, 1 );
		gl.viewport(0, 0, raptorjs.width, raptorjs.height);
		gl.clear( gl.COLOR_BUFFER_BIT )
			
		this.drawQuad(this.ssaoConvolutionShaderX, null);
		
		gl.bindFramebuffer(gl.FRAMEBUFFER, this.ambientOcclusionFinalFramebuffer.glFramebuffer );

		this.ssaoConvolutionShaderY.update();

		
		gl.clearColor( 1, 1, 1, 1 );
		gl.clear( gl.COLOR_BUFFER_BIT )

			
		this.drawQuad(this.ssaoConvolutionShaderY, null );
		
		
		//this.drawQuad(this.ssaoConvolutionShaderY, null );
		
		
		this.renderDeferred();
		//this.renderShadow();
		
		this.renderFXAA();
	}
	
	this.renderSceneTransparent();
	
	//eye, target, up
	gl.flush();
}
/*
 if( order == 0 )      gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0] + 1, lightPos[1], lightPos[2], 0, -1, 0 );
    else if( order == 1 ) gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0] - 1, lightPos[1], lightPos[2], 0, -1, 0 );
    else if( order == 2 ) gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0], lightPos[1] + 1, lightPos[2], 0, 0, 1 );
    else if( order == 3 ) gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0], lightPos[1] - 1, lightPos[2], 0, 0, -1 );
    else if( order == 4 ) gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0], lightPos[1], lightPos[2] + 1, 0, -1, 0 );
    else if( order == 5 ) gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0], lightPos[1], lightPos[2] - 1, 0, -1, 0 );
	glFrustum( -GL_width, GL_width, -GL_height, GL_height, zNear, zFar );
*/
raptorjs.renderSystem.prototype.renderShadowDepth = function( ) {
	
	gl.disableVertexAttribArray(1);
	gl.disableVertexAttribArray(2);
		
	for(var c = 0; c<this.shadowCameras.length; c++) {
		var shadowCamera = this.shadowCameras[c];
		var framebuffer = shadowCamera.framebuffer.glFramebuffer;

		//render shadow
		var shader = this.depthShader;


		//gl.bindFramebuffer(gl.FRAMEBUFFER, null);
		//gl.clear(gl.COLOR_BUFFER_BIT);
		gl.viewport(0, 0, raptorjs.width, raptorjs.height);
		

		gl.frontFace(gl.CCW);
		gl.enable(gl.DEPTH_TEST);
		
		var scene = this.scene;
		var entitys = scene.getEntitys();
		
		for(var e = 0;e<entitys.length;e++) {

			var entity = entitys[e];
			
			if(entity.type == "Actor") {
			
				var mesh = entity.mesh;
				var world = entity.getUpdatedWorldMatrix();
				//var viewProjection = raptorjs.mainCamera.worldViewProjection;
				
				gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
				gl.viewport(0, 0, framebuffer.width, framebuffer.height);
				
				shader.setUniform("viewProjection", raptorjs.matrix4.mul(world, shadowCamera.viewProjection)  );
				shader.setUniform("view", shadowCamera.view);
				shader.setUniform("world", world);
				shader.update();

				var attribute = shader.getAttributeByName('position');
				gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexPositionBuffer);
				gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);

				gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, mesh.vertexIndexBuffer);
				gl.drawElements( gl.TRIANGLES, mesh.vertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0 );  
			}
		}
		
	}
	
	gl.enableVertexAttribArray(1);
	gl.enableVertexAttribArray(2);
}

raptorjs.renderSystem.prototype.renderShadow = function( ) {
	
	this.shadowShader.update();
	
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	this.drawQuad( this.shadowShader, null );
	
}

raptorjs.renderSystem.prototype.createShadowCam = function( eye, target, up, face, sampler  ) {
	var width = 2048;
	var height = 2048;
	

	var width = 5;
	var height = width;
	var near = .01;
	var far = 100;
	
	var shadowCamera = {};

	shadowCamera.view 			= raptorjs.matrix4.lookAt( eye, target, up );
	shadowCamera.projection 	= raptorjs.matrix4.orthographic(-width, width, -height, height, near, far);
	shadowCamera.viewProjection = raptorjs.matrix4.mul(shadowCamera.view, shadowCamera.projection);
	shadowCamera.far 			= far;
	
	shadowCamera.framebuffer 			= raptorjs.createObject("framebuffer");
	shadowCamera.framebuffer.target 	= gl.TEXTURE_CUBE_MAP;
	shadowCamera.framebuffer.attachment = gl.DEPTH_ATTACHMENT;
	shadowCamera.framebuffer.face 		= face;
	shadowCamera.framebuffer.width 		= sampler.width;
	shadowCamera.framebuffer.height 	= sampler.height;
	
	shadowCamera.framebuffer.addSampler( sampler );
	shadowCamera.framebuffer.create();
	
	return shadowCamera;
}
/*
 if( order == 0 )      gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0] + 1, lightPos[1], lightPos[2], 0, -1, 0 );
    else if( order == 1 ) gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0] - 1, lightPos[1], lightPos[2], 0, -1, 0 );
    else if( order == 2 ) gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0], lightPos[1] + 1, lightPos[2], 0, 0, 1 );
    else if( order == 3 ) gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0], lightPos[1] - 1, lightPos[2], 0, 0, -1 );
    else if( order == 4 ) gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0], lightPos[1], lightPos[2] + 1, 0, -1, 0 );
    else if( order == 5 ) gluLookAt( lightPos[0], lightPos[1], lightPos[2], lightPos[0], lightPos[1], lightPos[2] - 1, 0, -1, 0 );
	glFrustum( -GL_width, GL_width, -GL_height, GL_height, zNear, zFar );
*/
raptorjs.renderSystem.prototype.createShadows = function( ) {
	/*
	var lightPos 		= [0, 15, 0];
	var shadowSampler 	= raptorjs.createObject("samplerCube");
	
	shadowSampler.internalformat = gl.RGB;
	shadowSampler.format 		 = gl.RGB;
	shadowSampler.type 			 = gl.FLOAT;
	shadowSampler.width			 = 1024;
	shadowSampler.height		 = 1024;
	
	var shadowCamera = this.createShadowCam(lightPos, raptorjs.vector3.add(lightPos, [1, 0, 0]), [0, -1, 0], gl.TEXTURE_CUBE_MAP_POSITIVE_X, shadowSampler);
	this.shadowCameras.push(shadowCamera);

	var shadowCamera = this.createShadowCam(lightPos, raptorjs.vector3.add(lightPos, [-1, 0, 0]), [0, -1, 0], gl.TEXTURE_CUBE_MAP_NEGATIVE_X, shadowSampler);
	this.shadowCameras.push(shadowCamera);

	var shadowCamera = this.createShadowCam(lightPos, raptorjs.vector3.add(lightPos, [0, 1, 0]), [0, 0, 1], gl.TEXTURE_CUBE_MAP_POSITIVE_Y, shadowSampler);
	this.shadowCameras.push(shadowCamera);
	
	var shadowCamera = this.createShadowCam(lightPos, raptorjs.vector3.add(lightPos, [0, -1, 0]), [0, 0, -1], gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, shadowSampler);
	this.shadowCameras.push(shadowCamera);
	
	var shadowCamera = this.createShadowCam(lightPos, raptorjs.vector3.add(lightPos, [0, 0, 1]), [0, -1, 0], gl.TEXTURE_CUBE_MAP_POSITIVE_Z, shadowSampler);
	this.shadowCameras.push(shadowCamera);
	
	var shadowCamera = this.createShadowCam(lightPos, raptorjs.vector3.add(lightPos, [0, 0, -1]), [0, -1, 0], gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, shadowSampler);
	this.shadowCameras.push(shadowCamera);
	
	//depth shader
	this.depthShader = raptorjs.createObject("shader");
	this.depthShader.definePragma("VARIANCE", (this.shadowType == "VARIANCE") ? 1 : 0 );
	this.depthShader.createFomFile("shaders/regularDepth.shader");
	this.depthShader.setUniform("far", shadowCamera.far );
	
	
	
	this.shadowShader = raptorjs.createObject("shader");
	this.shadowShader.createFomFile("shaders/shadow.shader");
	this.shadowShader.setUniform("viewProjection", this.quadViewProjection );
	this.shadowShader.setUniform("shadowSampler", shadowSampler );
	this.shadowShader.setUniform("far", shadowCamera.far );
	this.shadowShader.setUniform("lightViewProjection", shadowCamera.viewProjection );
	this.shadowShader.setUniform("lightPosition", lightPos );
	*/

}


raptorjs.renderSystem.prototype.renderScene = function( ) {
	
	var camera = raptorjs.mainCamera;

	gl.bindFramebuffer(gl.FRAMEBUFFER, this.deferredFramebuffer.glFramebuffer);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.viewport(0, 0, raptorjs.width, raptorjs.height);
	gl.frontFace(gl.CCW);
	gl.enable(gl.DEPTH_TEST);
	var scene = this.scene;
	var entitys = scene.getEntitys();
	
	for(var e = 0;e<entitys.length;e++) {
		var entity = entitys[e];
		
		if(entity.type == "Actor") {
		
			var mesh = entity.mesh;
			var material = mesh.material;
			
			if(material.alpha == 1.0) {
				
				var shader   = mesh.shader;
				
				var world = entity.getUpdatedWorldMatrix();
				var viewProjection = raptorjs.mainCamera.worldViewProjection;
				
				shader.setUniform("viewProjection", raptorjs.matrix4.mul(world, viewProjection)  );
				shader.setUniform("world", world);
				//shader.setUniform("metallic", material.metallic);
				shader.setUniform("specular", material.specular);
				
				if(material.displacementMaps.length > 0) 
					shader.setUniform("view",  raptorjs.mainCamera.view );
				
				shader.update();

				
				var attribute = shader.getAttributeByName('position');
				gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexPositionBuffer);
				gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);

				
				var attribute = shader.getAttributeByName('normal');
				gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexNormalBuffer);
				gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);

				if(material.normals.length > 0) {
					var attribute = shader.getAttributeByName('tangent');
					gl.bindBuffer(gl.ARRAY_BUFFER, mesh.tangentBuffer);
					gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);
				}
				

				var attribute = shader.getAttributeByName('textcoord');
				
				gl.bindBuffer(gl.ARRAY_BUFFER, mesh.textureCoordBuffer);
				gl.vertexAttribPointer(attribute, 2, gl.FLOAT, false, 0, 0);
				
				var texureID = 0;
				
				if( material.textures.length > 0 ) {
				
					var textureSampler = material.textures[0];
					var texture = textureSampler.getTexture();
					var textureUniform = shader.getUniformByName('diffuseSampler');
					
					gl.activeTexture( gl.TEXTURE0  );
					gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
					gl.uniform1i(textureUniform, 0);
					
					
				} else {
					//shader.setUniform("diffuseColor", material.diffuseColor);
				}
				
				if(material.normals.length > 0) {
					
					var textureSampler = material.normals[0];
					var texture = textureSampler.getTexture();
					var textureUniform = shader.getUniformByName('normalSampler');
					
					gl.activeTexture( gl.TEXTURE0 + 1);
					gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
					gl.uniform1i(textureUniform, 1);
					
				}
				
				if(material.displacementMaps.length > 0) {
					
					var textureSampler = material.displacementMaps[0];
					var texture = textureSampler.getTexture();
					var textureUniform = shader.getUniformByName('heightSampler');
					
					gl.activeTexture( gl.TEXTURE0 + 2);
					gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
					gl.uniform1i(textureUniform, 2);
					
					
				
					//var attribute = shader.getAttributeByName('bitangent');
					//gl.bindBuffer(gl.ARRAY_BUFFER, mesh.binormalBuffer);
					//gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);
					
					
				}
				
				//if(material.roughnessMaps.length > 0) {
					
				//	var textureSampler = material.roughnessMaps[0];
				//	var texture = textureSampler.getTexture();
				//	var textureUniform = shader.getUniformByName('roughnessSampler');
				//	
				//	gl.activeTexture( gl.TEXTURE0 + 3);
				//	gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
				//	gl.uniform1i(textureUniform, 3);
					
				//}
				
				
				

				gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, mesh.vertexIndexBuffer);
				gl.drawElements( gl.TRIANGLES, mesh.vertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0 );  
			
			}
		}
	}
	
	gl.disable(gl.DEPTH_TEST);
}


raptorjs.renderSystem.prototype.renderSceneTransparent = function( ) {
	
	var camera = raptorjs.mainCamera;

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	gl.clear(gl.FRAMEBUFFER);
	gl.viewport(0, 0, raptorjs.width, raptorjs.height);
	gl.frontFace(gl.CCW);
	
	var scene = this.scene;
	var entitys = scene.getEntitys();
	
	for(var e = 0;e<entitys.length;e++) {
		var entity = entitys[e];
		
		if(entity.type == "Actor") {
		
			var mesh = entity.mesh;
			var material = mesh.material;
			
			if(material.alpha != 1.0 || !this.deferred) {
				//console.log(entity.name);
				var shader   = mesh.forwardShader;
				
				var world = entity.getUpdatedWorldMatrix();
				var viewProjection = raptorjs.mainCamera.worldViewProjection;
				
				shader.setUniform("viewProjection", raptorjs.matrix4.mul(world, viewProjection)  );
				shader.setUniform("world", world);


			//	shader.setUniform("lightPosition", entity.getUpdatedWorldMatrix()[3].slice(0, 3), true ); 
			
				//shader.setUniform("attenuation",   entity.attenuation, true ); 
				//shader.setUniform("SourceRadius",  entity.sourceRadius, true ); 
				//shader.setUniform("SourceLength",  entity.sourceLength, true ); 
				shader.setUniform("cameraPosition", camera.eye );
								
				if(material.displacementMaps.length > 0) 
					shader.setUniform("view",  raptorjs.mainCamera.view );
				
				shader.update();

				
				var attribute = shader.getAttributeByName('position');
				gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexPositionBuffer);
				gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);

				
				var attribute = shader.getAttributeByName('normal');
				gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexNormalBuffer);
				gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);

				if(material.normals.length > 0) {
					var attribute = shader.getAttributeByName('tangent');
					gl.bindBuffer(gl.ARRAY_BUFFER, mesh.tangentBuffer);
					gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);
				}
				

				var attribute = shader.getAttributeByName('textcoord');
				
				gl.bindBuffer(gl.ARRAY_BUFFER, mesh.textureCoordBuffer);
				gl.vertexAttribPointer(attribute, 2, gl.FLOAT, false, 0, 0);
				
				var texureID = 0;
				
				if( material.textures.length > 0 ) {
				
					var textureSampler = material.textures[0];
					var texture = textureSampler.getTexture();
					var textureUniform = shader.getUniformByName('diffuseSampler');
					
					gl.activeTexture( gl.TEXTURE0  );
					gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
					gl.uniform1i(textureUniform, 0);
					
					
				} else {
					//shader.setUniform("diffuseColor", material.diffuseColor);
				}
				
				if(material.normals.length > 0) {
					
					var textureSampler = material.normals[0];
					var texture = textureSampler.getTexture();
					var textureUniform = shader.getUniformByName('normalSampler');
					
					gl.activeTexture( gl.TEXTURE0 + 1);
					gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
					gl.uniform1i(textureUniform, 1);
					
				}
				
				if(material.displacementMaps.length > 0) {
					
					var textureSampler = material.displacementMaps[0];
					var texture = textureSampler.getTexture();
					var textureUniform = shader.getUniformByName('heightSampler');
					
					gl.activeTexture( gl.TEXTURE0 + 2);
					gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
					gl.uniform1i(textureUniform, 2);
					
					
				
					//var attribute = shader.getAttributeByName('bitangent');
					//gl.bindBuffer(gl.ARRAY_BUFFER, mesh.binormalBuffer);
					//gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);
					
					
				}
				
				//if(material.roughnessMaps.length > 0) {
					
				//	var textureSampler = material.roughnessMaps[0];
				//	var texture = textureSampler.getTexture();
				//	var textureUniform = shader.getUniformByName('roughnessSampler');
				//	
				//	gl.activeTexture( gl.TEXTURE0 + 3);
				//	gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
				//	gl.uniform1i(textureUniform, 3);
					
				//}
				
					
				gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
				gl.enable(gl.BLEND);
				gl.disable(gl.DEPTH_TEST);
				gl.enable(gl.DEPTH_TEST);
				gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, mesh.vertexIndexBuffer);
				gl.drawElements( gl.TRIANGLES, mesh.vertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0 );  
				
				gl.enable(gl.DEPTH_TEST);
				gl.disable(gl.BLEND);
			}
		}
	}
}



raptorjs.renderSystem.prototype.renderDeferred = function( ) {
	var camera = raptorjs.mainCamera;
	var scene = this.scene;
	var entitys = scene.getEntitys();
	var deferredShader = this.deferredShader;
	
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.fxaaFramebuffer.glFramebuffer );
	//gl.bindFramebuffer(gl.FRAMEBUFFER, null );
	deferredShader.setUniform("cameraPosition", camera.eye );
	
	gl.clearColor( 0, 0, 0, 1 );
	gl.viewport(0, 0, raptorjs.width, raptorjs.height);
	gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );

	//deferredShader.setUniform("InvProjection", raptorjs.matrix4.inverse( raptorjs.mainCamera.projection )  );
	deferredShader.update();
	
	
	gl.disable(gl.DEPTH_TEST);
	gl.enable(gl.BLEND);
	
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE);

	var lights = 0;
	
	for(var e = 0;e<entitys.length;e++) {
		var entity = entitys[e];
		if(entity.type == "PointLight") {
			if(lights < 1) {
				//console.log("there is light");
				
				
				gl.clearColor( 0, 0, 0, 1 );
				
				/*
					var textureSampler = this.reflectionSampler;
					var texture = textureSampler.cubeTexture;
					var textureUniform = deferredShader.getUniformByName('reflectionSampler');
					
					gl.activeTexture( gl.TEXTURE0 + textureSampler.id);
					gl.bindTexture( textureSampler.target, texture );
					gl.uniform1i(textureUniform, textureSampler.id);
				*/
				deferredShader.setUniform("attenuation",   entity.attenuation, true ); 
				deferredShader.setUniform("SourceRadius",  entity.sourceRadius, true ); 
				deferredShader.setUniform("SourceLength",  entity.sourceLength, true ); 
				//deferredShader.setUniform("metallic",   entity.mesh.material.metallic, true ); 
				deferredShader.setUniform("lightPosition", entity.getUpdatedWorldMatrix()[3].slice(0, 3), true ); 
			
				this.drawQuad( deferredShader, null );
				
				lights++;
		}
		}
	
	}
	
	gl.disable(gl.BLEND);
	gl.enable(gl.DEPTH_TEST);

}

raptorjs.renderSystem.prototype.renderAmbientOcclusion = function( ) {
		
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.ambientOcclusionFramebuffer.glFramebuffer );

	var m = raptorjs.mainCamera.view;
	
	var view3x3 = [	[m[0][0], m[1][0], m[2][0]],
					[m[1][1], m[1][2], m[1][2]],
					[m[2][0], m[2][1], m[2][2]] ];


	this.ambientocclusionShader.update();
	
	gl.clearColor( 1, 1, 1, 1 );
	gl.viewport(0, 0, raptorjs.width, raptorjs.height);
	gl.clear( gl.COLOR_BUFFER_BIT )
	gl.frontFace(gl.CCW);
	
	this.drawQuad( this.ambientocclusionShader, null );
}

raptorjs.renderSystem.prototype.renderFXAA = function( ) {

	gl.bindFramebuffer(gl.FRAMEBUFFER, null );	
	//gl.enable(gl.DEPTH_TEST);
	gl.disable(gl.BLEND);
	
	gl.viewport(0, 0, raptorjs.width, raptorjs.height);
	
	this.fxaaShader.update();
	
	this.drawQuad( this.fxaaShader, null );
	
	

	
}


/**
 * draw quad 
 * @param {(shaderObject)} shader
 * @param {(framebuffer)} framebuffer
 * @param {(boolean)} don't update
**/
raptorjs.renderSystem.prototype.drawQuad = function(shader, framebuffer, noUpdate) {
	var quadVertices = this.quadVertices;
	var quadIndices = this.quadIndices;
	var quadUv = this.quadUv;
	
	

	gl.useProgram( shader.program );

	//if(!noUpdate)
	//	shader.update();

	var attribute = shader.getAttributeByName('position');
	gl.bindBuffer(gl.ARRAY_BUFFER, quadVertices);
	gl.vertexAttribPointer(attribute, quadVertices.itemSize, gl.FLOAT, false, 0, 0);

	var attribute = shader.getAttributeByName('uv');
	gl.bindBuffer(gl.ARRAY_BUFFER, quadUv);
	gl.vertexAttribPointer(attribute, quadUv.itemSize, gl.FLOAT, false, 0, 0);
	
	gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, quadIndices );
	gl.drawElements( gl.TRIANGLES, quadIndices.numItems, gl.UNSIGNED_SHORT, 0 );
}

/**
 * create deferred buffers
**/
raptorjs.renderSystem.prototype.createQuad = function() {
	var plane = raptorjs.primitives.createPlane(2, 2, 1, 1);
	
	this.quadView = raptorjs.matrix4.lookAt([0, 0, 0], [0, -1, 0], [0, 0, -1]);
	this.quadProjection = raptorjs.matrix4.orthographic(-1, 1, -1, 1, -1, 1);
	this.quadViewProjection = raptorjs.matrix4.mul(this.quadView, this.quadProjection);
	
	this.quadVertices = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, this.quadVertices);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(plane.vertexBuffer.data), gl.STATIC_DRAW);
	this.quadVertices.name = 'position';
	this.quadVertices.itemSize = 3;
	this.quadVertices.numItems = plane.vertexBuffer.data.length / 3;
	
	this.quadUv = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, this.quadUv);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(plane.uvBuffer.data), gl.STATIC_DRAW);
	this.quadUv.name = 'uv';
	this.quadUv.itemSize = 2;
	this.quadUv.numItems = plane.uvBuffer.data.length / 2;

	
	this.quadIndices = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.quadIndices);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(plane.indexBuffer.data), gl.STATIC_DRAW);
	this.quadIndices.name = 'index';
	this.quadIndices.itemSize = 1;
	this.quadIndices.numItems = plane.indexBuffer.data.length;
}




raptorjs.renderSystem.prototype.createMultipleRenderTargets = function( ) {

	var def = raptorjs.resources.getTexture("default");

	var positive_x = def;

	 this.reflectionSampler = raptorjs.createObject("samplerCube");

	 this.reflectionSampler.addTexture(raptorjs.resources.getTexture("positive_x"), gl.TEXTURE_CUBE_MAP_POSITIVE_X);
	 this.reflectionSampler.addTexture(raptorjs.resources.getTexture("negative_x"), gl.TEXTURE_CUBE_MAP_NEGATIVE_X);
	 this.reflectionSampler.addTexture(raptorjs.resources.getTexture("negative_y"), gl.TEXTURE_CUBE_MAP_POSITIVE_Y);
	 this.reflectionSampler.addTexture(raptorjs.resources.getTexture("positive_y"), gl.TEXTURE_CUBE_MAP_NEGATIVE_Y);
	 this.reflectionSampler.addTexture(raptorjs.resources.getTexture("positive_z"), gl.TEXTURE_CUBE_MAP_POSITIVE_Z);
	 this.reflectionSampler.addTexture(raptorjs.resources.getTexture("negative_z"), gl.TEXTURE_CUBE_MAP_NEGATIVE_Z);


	this.deferredFramebuffer = raptorjs.createObject("framebuffer");
	this.deferredFramebuffer.width = raptorjs.width;
	this.deferredFramebuffer.height = raptorjs.height;
	//this.deferredFramebuffer.target = gl.TEXTURE_CUBE_MAP;
	
	var diffuseSampler = raptorjs.createObject("sampler2D");
	var normalSampler = raptorjs.createObject("sampler2D");
	this.infoSampler = raptorjs.createObject("sampler2D");
	var info2Sampler = raptorjs.createObject("sampler2D");
	
	diffuseSampler.type = gl.FLOAT;
	normalSampler.type = gl.FLOAT;
	this.infoSampler.type = gl.FLOAT;
	info2Sampler.type = gl.FLOAT;
	
	this.deferredFramebuffer.addSampler(diffuseSampler);
	this.deferredFramebuffer.addSampler(normalSampler);
	this.deferredFramebuffer.addSampler(this.infoSampler);
	//this.deferredFramebuffer.addSampler(info2Sampler);

	this.deferredFramebuffer.create();

	console.log("Renderbuffer", this.deferredFramebuffer);

	this.deferredShader = raptorjs.createObject("shader");
	this.deferredShader.createFomFile("shaders/deferred.shader");
	this.deferredShader.setUniform("viewProjection", this.quadViewProjection );
	this.deferredShader.setUniform("reflectionSampler", this.reflectionSampler );
	this.deferredShader.setUniform("diffuseSampler", diffuseSampler);
	this.deferredShader.setUniform("normalSampler", normalSampler);
	this.deferredShader.setUniform("infoSampler", this.infoSampler);
	//this.deferredShader.setUniform("info2Sampler", info2Sampler);
	this.deferredShader.setUniform("metallic", .7);


	this.deferredShader.setUniform("lightPosition", [0,0.4,0]);
	this.deferredShader.setUniform("SpotAngles", [30, 0.1]);
	this.deferredShader.setUniform("LightDirection", [0.18, .2, 5.5] );
	this.deferredShader.setUniform("LightColor", [1, 1, 1] );
	this.deferredShader.setUniform("far", raptorjs.mainCamera.far );
	
	
	
	this.deferredShader.setUniform("attenuation", 1 );
	
	//this.deferredShader.setUniform("properties_1", [0.1, 1.0, 1.0, 1.0] );
		
		
	var randomTexure = raptorjs.resources.getTexture("random");
	var randomSampler = raptorjs.createObject("sampler2D");
	
	randomSampler.addTexture(randomTexure);
	
	this.deferredShader.setUniform("randomSampler", randomSampler );	

	this.fxaaFramebuffer = raptorjs.createObject("framebuffer");
	this.fxaaFramebuffer.width = raptorjs.width;
	this.fxaaFramebuffer.height = raptorjs.height;
	
	var deferredSampler = raptorjs.createObject("sampler2D");
	deferredSampler.type = gl.FLOAT;
	
	this.fxaaFramebuffer.addSampler(deferredSampler);
	this.fxaaFramebuffer.create();
	
	this.fxaaShader = raptorjs.createObject("shader");
	this.fxaaShader.createFomFile("shaders/fxaa.shader");
	
	this.fxaaShader.setUniform("viewProjection", this.quadViewProjection );
	this.fxaaShader.setUniform("deferredSampler", deferredSampler );
	this.fxaaShader.setUniform("res", [raptorjs.width, raptorjs.height] );
	
	
	var ambientOcclusionSampler = raptorjs.createObject("sampler2D");
	ambientOcclusionSampler.type = gl.FLOAT;
	
	this.ambientOcclusionFramebuffer = raptorjs.createObject("framebuffer");
	this.ambientOcclusionFramebuffer.width = raptorjs.width;
	this.ambientOcclusionFramebuffer.height = raptorjs.height;
	
	this.ambientOcclusionFramebuffer.addSampler( ambientOcclusionSampler );
	this.ambientOcclusionFramebuffer.create();
	
	//this.deferredShader.setUniform("sampler", ambientOcclusionSampler );

	var randomTexure = raptorjs.resources.getTexture("random");
	var randomSampler = raptorjs.createObject("sampler2D");
	
	randomSampler.addTexture(randomTexure);
	
	
	
	this.ssaoConvolutionShaderX = raptorjs.createObject("shader");
	this.ssaoConvolutionShaderX.createFomFile("shaders/convolution.shader");
	this.ssaoConvolutionShaderX.setUniform("viewProjection", this.quadViewProjection);
	this.ssaoConvolutionShaderX.setUniform("imageIncrement", [1 / raptorjs.width, 0] );
	this.ssaoConvolutionShaderX.setUniform("image", ambientOcclusionSampler );
	this.ssaoConvolutionShaderX.setUniform("far", raptorjs.mainCamera.far );
	this.ssaoConvolutionShaderX.setUniform("random", randomSampler );
	
	
	var ssaoConvolutionXSampler = raptorjs.createObject("sampler2D");
	ssaoConvolutionXSampler.type = gl.FLOAT;
	
	this.ambientOcclusionXFramebuffer = raptorjs.createObject("framebuffer");
	this.ambientOcclusionXFramebuffer.width = raptorjs.width;
	this.ambientOcclusionXFramebuffer.height = raptorjs.height;
	
	this.ambientOcclusionXFramebuffer.addSampler( ssaoConvolutionXSampler );
	this.ambientOcclusionXFramebuffer.create();
	
	var randomTexure = raptorjs.resources.getTexture("random");
	var randomSampler = raptorjs.createObject("sampler2D");
	
	randomSampler.addTexture(randomTexure);
	
	this.ssaoConvolutionShaderY = raptorjs.createObject("shader");
	this.ssaoConvolutionShaderY.createFomFile("shaders/convolution.shader");
	this.ssaoConvolutionShaderY.setUniform("viewProjection", this.quadViewProjection );
	this.ssaoConvolutionShaderY.setUniform("imageIncrement", [0, 1 / raptorjs.height] );
	this.ssaoConvolutionShaderY.setUniform("image", ssaoConvolutionXSampler );
	this.ssaoConvolutionShaderY.setUniform("far", raptorjs.mainCamera.far );
	this.ssaoConvolutionShaderY.setUniform("random", randomSampler );
	
	
	var ssaoConvolutionFinalSampler = raptorjs.createObject("sampler2D");
	ssaoConvolutionFinalSampler.type = gl.FLOAT;
	
	this.ambientOcclusionFinalFramebuffer = raptorjs.createObject("framebuffer");
	this.ambientOcclusionFinalFramebuffer.width = raptorjs.width;
	this.ambientOcclusionFinalFramebuffer.height = raptorjs.height;
	
	this.ambientOcclusionFinalFramebuffer.addSampler( ssaoConvolutionFinalSampler );
	this.ambientOcclusionFinalFramebuffer.create();
	
	
	this.ambientocclusionShader = raptorjs.createObject("shader");
	this.ambientocclusionShader.createFomFile("shaders/ambientOcclusionFast.shader");
	this.ambientocclusionShader.setUniform("viewProjection", this.quadViewProjection );
	this.ambientocclusionShader.setUniform("normalDepthSampler", normalSampler );
	this.ambientocclusionShader.setUniform("screenWidth", raptorjs.width );
	this.ambientocclusionShader.setUniform("screenHeight", raptorjs.height );
	this.ambientocclusionShader.setUniform("parameters", [ 0.15, 0.47, 0.5, 1.2 ] );
	
	var randomTexure = raptorjs.resources.getTexture("random");
	var randomSampler = raptorjs.createObject("sampler2D");
	
	randomSampler.addTexture(randomTexure);
	
	this.ambientocclusionShader.setUniform("randomSampler", randomSampler );
	
	var vec3 = raptorjs.vector3;
	var vec4 = raptorjs.vector4;
	var radius = 0.02;
	
	var scale = [ vec3.scale( vec3(-0.556641,-0.037109,-0.654297), radius ), 
				vec3.scale( vec3(0.173828,0.111328,0.064453), radius ), 
				vec3.scale( vec3(0.001953,0.082031,-0.060547), radius ), 
				vec3.scale( vec3(0.220703,-0.359375,-0.062500), radius ), 
				vec3.scale( vec3(0.242188,0.126953,-0.250000), radius ), 
				vec3.scale( vec3(0.070313,-0.025391,0.148438), radius ), 
				vec3.scale( vec3(-0.078125,0.013672,-0.314453), radius ), 
				vec3.scale( vec3(0.117188,-0.140625,-0.199219), radius ), 
				vec3.scale( vec3(-0.251953,-0.558594,0.082031), radius ), 
				vec3.scale( vec3(0.308594,0.193359,0.324219), radius ), 
				vec3.scale( vec3(0.173828,-0.140625,0.031250), radius ), 
				vec3.scale( vec3(0.179688,-0.044922,0.046875), radius ), 
				vec3.scale( vec3(-0.146484,-0.201172,-0.029297), radius ), 
				vec3.scale( vec3(-0.300781,0.234375,0.539063), radius ), 
				vec3.scale( vec3(0.228516,0.154297,-0.119141), radius ), 
				vec3.scale( vec3(-0.119141,-0.003906,-0.066406), radius ), 
				vec3.scale( vec3(-0.218750,0.214844,-0.250000), radius ), 
				vec3.scale( vec3(0.113281,-0.091797,0.212891), radius ), 
				vec3.scale( vec3(0.105469,-0.039063,-0.019531), radius ), 
				vec3.scale( vec3(-0.705078,-0.060547,0.023438), radius ), 
				vec3.scale( vec3(0.021484,0.326172,0.115234), radius ), 
				vec3.scale( vec3(0.353516,0.208984,-0.294922), radius ), 
				vec3.scale( vec3(-0.029297,-0.259766,0.089844), radius ), 
				vec3.scale( vec3(-0.240234,0.146484,-0.068359), radius ), 
				vec3.scale( vec3(-0.296875,0.410156,-0.291016), radius ), 
				vec3.scale( vec3(0.078125,0.113281,-0.126953), radius ), 
				vec3.scale( vec3(-0.152344,-0.019531,0.142578), radius ), 
				vec3.scale( vec3(-0.214844,-0.175781,0.191406), radius ), 
				vec3.scale( vec3(0.134766,0.414063,-0.707031), radius ), 
				vec3.scale( vec3(0.291016,-0.833984,-0.183594), radius ), 
				vec3.scale( vec3(-0.058594,-0.111328,0.457031), radius ), 
				vec3.scale( vec3(-0.115234,-0.287109,-0.259766), radius ) ];
				
	var kernelRad = [[1.163003/radius,4.624262/radius,9.806342/radius,2.345541/radius], 
					[2.699039/radius,6.016871/radius,3.083554/radius,3.696197/radius], 
					[1.617461/radius,2.050939/radius,4.429457/radius,5.234036/radius], 
					[3.990876/radius,1.514475/radius,3.329241/radius,7.328508/radius], 
					[2.527725/radius,3.875453/radius,8.760140/radius,1.412308/radius], 
					[2.885205/radius,1.977866/radius,3.617674/radius,3.453552/radius], 
					[1.712336/radius,5.341163/radius,4.771728/radius,2.965737/radius], 
					[1.204293/radius,1.108428/radius,2.109570/radius,2.475453/radius] ]; 
	

	this.ambientocclusionShader.setUniform("scale", scale );
	this.ambientocclusionShader.setUniform("kernelRad", kernelRad );
	this.ambientocclusionShader.setUniform("far", raptorjs.mainCamera.far );
	this.ambientocclusionShader.setUniform("positionSampler", this.infoSampler );
	this.ambientocclusionShader.setUniform("positionSampler", this.infoSampler );
	
	
	
	this.deferredShader.setUniform("ambientOcclusionSampler", ssaoConvolutionFinalSampler );
	
	


var  ssaoKernel = [];
for (var i = 0; i < 64; ++i)
{
   var sample = [
        Math.random() * 2.0 - 1.0, 
        Math.random() * 2.0 - 1.0, 
        Math.random()
    ];
	
    sample = raptorjs.vector3.normalize(sample);
    sample *= Math.random();
	
    scale = i / 64.0; 
	
    ssaoKernel.push(sample);  
}
	this.ambientocclusionShader.setUniform("samples", ssaoKernel );
	
	
raptorjs.system.ambientocclusionShader.setUniform("parameters", [0.6, 0.075, 1.0, 1.0] );
	
}



/**
 * initialize webgl
 * @param {(Dom canvas)} canvas
**/
raptorjs.renderSystem.prototype.initializeWebgl = function(canvas) {
	try {
	
		var width = canvas.offsetWidth;
		var height = canvas.offsetHeight;
		
		canvas.width = raptorjs.math.nextHighestPowerOfTwo(width);
		canvas.height = raptorjs.math.nextHighestPowerOfTwo(height);
		
		console.log('adjusted resolution to width:', canvas.width , 'height', canvas.height);
		
		raptorjs.canvas = canvas;
		
		raptorjs.setLoadingText("Init Webgl");
		
		gl = canvas.getContext("webgl");//canvas.getContext("experimental-webgl",  {alpha: false});

		raptorjs.extensions.textureFloat = gl.getExtension('OES_texture_float');
		raptorjs.extensions.textureHalf = gl.getExtension('OES_texture_half_float');
		raptorjs.extensions.elementIndexUint = gl.getExtension('OES_element_index_uint'); 
		raptorjs.extensions.derivatives = gl.getExtension('OES_standard_derivatives');
		raptorjs.extensions.texture3d = gl.getExtension("OES_texture_3D");
		raptorjs.extensions.depthTexture = gl.getExtension("WEBKIT_WEBGL_depth_texture");
		raptorjs.extensions.anisotropic = gl.getExtension("WEBKIT_EXT_texture_filter_anisotropic");
		raptorjs.extensions.textureCompression = gl.getExtension("WEBKIT_WEBGL_compressed_texture_s3tc");
		raptorjs.extensions.OES_half_float_linear = gl.getExtension("OES_half_float_linear");
		raptorjs.extensions.WEBGL_draw_buffers = gl.getExtension("WEBGL_draw_buffers");
		raptorjs.extensions.WEBGL_color_buffer_float = gl.getExtension("WEBGL_color_buffer_float");
		raptorjs.extensions.EXT_sRGB = gl.getExtension("EXT_sRGB");
	
		raptorjs.setLoadingText("Init RaptorEngine");
		
		if(raptorjs.extensions.elementIndexUint)
			this.indexType = gl.UNSIGNED_INT;
		else
			this.indexType = gl.UNSIGNED_SHORT;
		
		
		var formats = gl.getParameter(gl.COMPRESSED_TEXTURE_FORMATS);
		 
		raptorjs.extensions.dxt5Supported = false;
		
		for(var i = 0; i<formats.length; i++) {

			if(formats[i] == raptorjs.extensions.textureCompression.COMPRESSED_RGBA_S3TC_DXT5_EXT) {
				raptorjs.extensions.dxt5Supported = true;
			}
		}
		 
		console.log(raptorjs.extensions, formats);
 		
		gl.viewportWidth = canvas.width;
		gl.viewportHeight = canvas.height;
		
		raptorjs.width = canvas.width;
		raptorjs.height = canvas.height;
		raptorjs.canvas = canvas;

	} catch (e) {  }
	
	if (!gl) {
		alert("Could not initialise WebGL, sorry :-(");
	}
}


/**
 * set graphics library
 * @param {(string)} name
**/
raptorjs.renderSystem.prototype.setGraphicsLibrary = function(name) {
	/*
		
	*/
}

/**
 * Calculate elapsed time and current time
 */
raptorjs.renderSystem.prototype.smoothTimeEvolution = function() {
	if(!raptorjs.firstTime)
		raptorjs.firstTime = new Date().getTime();

	var timeNow = new Date().getTime() - raptorjs.firstTime;
	if (raptorjs.lastTime != 0) {
		var elapsed = timeNow - raptorjs.lastTime;
		raptorjs.elapsed = elapsed / 1000;
		raptorjs.timeNow = timeNow;
	}
	raptorjs.lastTime = timeNow;
}


/**
 * isFloat
 * @param {array} array
 * @param {int} isfloat
 */
function isFloat(array, isfloat) {
	for(var c = 0; c < array.length; c++) 
	{
		var child = array[c];
		if(typeof child == "object") 
		{
			getFirstvalue(child, isfloat);
		} else {
			if(isFloatCheck(child))
				return true;
		}
	}
	return isfloat;
}


/**
 * get first value
 * @param {array} array
 * @param {int} isInt
 */
function getFirstvalue (array, isInt) {
	if(typeof array == "object") {
			return getFirstvalue(array[0]);
	} else {
		return array;
	}
}


/**
 * check if object is integer
 * @param {object} 
 */
function isInteger(f) {
    return typeof(f)==="number" && Math.round(f) == f;
}


/**
 * check if is float
 * @param {object} 
 */
function isFloatCheck(f) { 
	return typeof(f)==="number" && !isInteger(f); 
}

raptorjs.renderSystem.prototype.getFrustumCorners = function(projection, view, world) {
	
	var viewClone = raptorjs.matrix4.copyMatrix(view);
	viewClone = raptorjs.matrix4.setTranslation(viewClone, [0,0,0]);
	
	if(world) {
		var viewProjection = raptorjs.matrix4.inverse( raptorjs.matrix4.composition(projection, viewClone) );
	} else {
		var viewProjection = raptorjs.matrix4.inverse( projection );
	}
	
	var corners = [];

	for(var c =0; c < this.cornerVertices.length;c++)
	{
		var vert = this.cornerVertices[c];
		
		vert.push(0.0);
		
		vert = raptorjs.matrix4.transformPoint(viewProjection, vert);

		corners.push(vert);
	}
	
	return corners;
};