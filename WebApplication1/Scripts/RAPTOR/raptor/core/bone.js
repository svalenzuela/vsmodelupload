raptorjs.bone = function(){
	this.isRoot;
	this.name;
	this.parent;
	this.children = [];
	
	this.transformation;
	this.globalTransformation;
	
	this.offsetMatrix;
	
	this.positionkeys;
	this.rotationkeys;
	
	this.currentAnimationMatrix;
	
	this.entity;
}

raptorjs.bone.prototype.addChild = function( bone ) {
	this.children.push( bone );
}