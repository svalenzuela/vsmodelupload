raptorjs.skeleton = function(){
	this.root;
	this.bones = [];
	this.name;
	
	this.rootTransformation;
	
	this.delta = 0;
	this.currentAnimationMatrix;
}

raptorjs.skeleton.prototype.parse = function( boneInfo, rootNode ) {
	var bone = raptorjs.createObject("bone");
	
	this.rootTransformation = raptorjs.matrix4.fromArray( rootNode.transformation );
	
	bone.isRoot = true;
	bone.transformation = raptorjs.matrix4.fromArray( boneInfo.transformation );
	bone.globalTransformation = bone.transformation;
	
	this.bones.push(bone);
	this.root = bone;
	
	
	for(var c = 0; c < boneInfo.children.length; c++) {
		
		this.parseChild(bone, boneInfo.children[c]);
		
	}

}

raptorjs.skeleton.prototype.parseChild = function( parentBone, childBoneInfo ) {
	var bone = raptorjs.createObject("bone");
	
	bone.parent = parentBone;
	bone.transformation = raptorjs.matrix4.fromArray( childBoneInfo.transformation );
	bone.name = childBoneInfo.name;
	bone.globalTransformation = raptorjs.matrix4.mul( bone.transformation,  parentBone.globalTransformation );
	
	this.bones.push(bone);
	
	parentBone.addChild(bone);
	
	//parse Children
	if(childBoneInfo.children)
	for(var c = 0; c < childBoneInfo.children.length; c++) {
		
		this.parseChild(bone, childBoneInfo.children[c]);
		
	}
	
}

raptorjs.skeleton.prototype.getBoneByName = function( name  ) {
	var bones = this.bones;
	for(var c = 0; c < bones.length; c++) {
		var bone = bones[c];

		if(bone.name == name)
			return bone;
		
	}
	
}

raptorjs.skeleton.prototype.parseAnimation = function( channels ) {
	for(var c = 0; c < channels.length; c++) {
		var channel = channels[c];
		var name = channel.name;
		
		var bone = this.getBoneByName(name);
		
		if(bone) {
			bone.positionkeys = channel.positionkeys;
			bone.rotationkeys = channel.rotationkeys;
		}
	}
}

raptorjs.skeleton.prototype.animate = function( bone, id ) {
	bone.currentAnimationMatrix = raptorjs.matrix4.identity();
	
	if(bone.positionkeys) {
		
		
		if(bone.positionkeys[id]) {
			var currentTranslation = bone.positionkeys[id][1];
			bone.currentAnimationMatrix = raptorjs.matrix4.setTranslation(bone.currentAnimationMatrix, currentTranslation);
		}
		
		
		if(bone.rotationkeys[id]) {
			var currentRotation = bone.rotationkeys[id][1];
			bone.currentAnimationMatrix = raptorjs.matrix4.rotateZYX(bone.currentAnimationMatrix, currentRotation);
		
		}
		//console.log(bone.currentAnimationMatrix);
		
		if(bone.parent)
			bone.globalTransformation = raptorjs.matrix4.mul( bone.currentAnimationMatrix,  bone.parent.globalTransformation );
		//else	
		//	bone.globalTransformation = raptorjs.matrix4.identity();
	}

	
	return bone;
	
}