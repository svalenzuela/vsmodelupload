/**
 * 	Raptor Engine - Core
 * 	Copyright (c) 2013 RAPTORCODE STUDIOS
 * 	All rights reserved.
 *
 * 	Author: Kaj Dijksta
 *
 **/

	var raptorjs = raptorjs || {};
	var gl;
	
	//core vars
	raptorjs.math;
	raptorjs.resourceManager;
	raptorjs.events;
	raptorjs.mainCamera;
	raptorjs.mainPlayer;
	raptorjs.client = {};
	raptorjs.elapsed = 0;
	raptorjs.timeNow = 0.01;
	raptorjs.extensions = {};
	raptorjs.canvas;
	raptorjs.engine;
	raptorjs.resources;
    raptorjs.lastTime = 0;
	raptorjs.lastTimeCheck = 0;

	
	//plugin related vars
	raptorjs.seaInstance;
	raptorjs.skyInstance;
	raptorjs.grayImage;
	raptorjs.sunLight;
	raptorjs.errorTexture;
	raptorjs.planet;
	raptorjs.oceanInstance;
	raptorjs.pointSprite;
	raptorjs.logo;
	raptorjs.ocean;
	
	/**
	 * Application
	 */
	raptorjs.application = function ( ) {
	
		//	allocate Objects
		raptorjs.mainPlayer = raptorjs.createObject("player");
		raptorjs.events = raptorjs.createObject("eventManager");
		raptorjs.scene = raptorjs.createObject("sceneNode");
		raptorjs.mainCamera = raptorjs.createObject("camera");
		raptorjs.resources = raptorjs.createObject("resourceManager");
		
		
		
		//	create Error texture for when a texture is not present
		raptorjs.createErrorTexture();
		
		raptorjs.system.createDeferredBuffers();
		
		//	set camera and scene
		raptorjs.system.setCamera( raptorjs.mainCamera );
		raptorjs.system.setScene( raptorjs.scene );
		
	
		//	gets called when everything is loaded
		raptorjs.resources.finishCallback = function( ) {	
				
			if(raptorjs.build)
				raptorjs.system.loadMeshFromFile('sponza.dae', 'sponza');
			else
				raptorjs.system.loadMeshFromJSON('sponza', 'sponza',24);
			
			raptorjs.mainCamera.update();
			raptorjs.mainPlayer.update();
			
			if(raptorjs.build)
				raptorjs.resources.loadNextTexture();
	
			raptorjs.system.ready = true;
			
			//	Setup render pipeline
			raptorjs.system.createFramebuffers();
			raptorjs.system.createBuffers();
			raptorjs.system.createShadowMaps();
			
			if(raptorjs.build)
				raptorjs.system.finalizeLoadedMeshes();
			else
				raptorjs.system.createMeshFromJSON();
			
			raptorjs.ocean = raptorjs.createObject("ocean");
			
			raptorjs.startRenderPipeline();
		}
		
		
		//	load extra resources
		console.log( basepath+" ***")
		raptorjs.resources.addTexture(basepath + "media/textures/white.png", 'white');
		raptorjs.resources.addTexture(basepath + "media/textures/background.png", 'loadingBackground');
		/*
			raptorjs.resources.addTexture("media/textures/random.png", 'random');
			raptorjs.resources.addTexture("media/textures/ssao.png", 'ssao');
			raptorjs.resources.addTexture("media/textures/rotrandom.png", 'rotrandom');
			raptorjs.resources.addTexture("media/textures/Unigine01.png", 'unigine');
			raptorjs.resources.addTexture("media/textures/noise.png", 'noise');
			raptorjs.resources.addTexture("media/textures/SMAA/AreaTex.png", 'AreaTex');
			raptorjs.resources.addTexture("media/textures/SMAA/SearchTex.png", 'SearchTex');
			raptorjs.resources.addTexture("media/textures/ssao.png", 'ssao');
			raptorjs.resources.addTexture("media/textures/randomRotation.png", 'randomRotation');
		*/
		
		raptorjs.resources.loadNextTexture();
    }
	
	
	/**
	 * Render callback gets called every frame
	 */
    raptorjs.renderCallback = function ( ) {
	
		if(raptorjs.system.ready) {
		
			//	update objects
			raptorjs.mainPlayer.update();
			raptorjs.mainCamera.update();
			//raptorjs.system.updateBuffers();
			raptorjs.system.render();
			raptorjs.render();
			//raptorjs.lightshafts.update();
			
		} else {
			raptorjs.logo.update();
		}
		
		//raptorjs.oceanInstance.pipeline();
		//raptorjs.planet.update();
		//raptorjs.oceanInstance.drawQuad(raptorjs.ocean.shader, null);
    }
	
	
	/**
	 * Initialize raptorEngine framework
	 */
	raptorjs.initialize =  function () {
        var canvas = document.getElementById("raptorEngine");

		raptorjs.system = raptorjs.createObject("renderSystem");
		raptorjs.system.setGraphicsLibrary("WebGL_1.0");
        raptorjs.system.initializeWebgl(canvas);
		
		raptorjs.application();
    }
	
	
	/**
	 * create raptorjs object
	 * @param {string} name of class
	 */
	raptorjs.createObject = function(className) {
		var instance =  raptorjs[className];
		var object = new instance;
		return object;
		if (typeof instance != 'function') {
			throw 'raptorjs doesnt seem to contain the namespace: ' + className
		} else {
			return object;
		}
	};
	
	
	/**
	 * Clear runtime (Depricated)
	 */
	window.unload = function( ) {
		
	}
	
	
	/**
	 * load url
	 * @param {string} url you want to load
	 */
	raptorjs.loadTextFileSynchronous = function(url) {
		var request;
		if (window.XMLHttpRequest) {
			request = new XMLHttpRequest();
			if (request.overrideMimeType) {
			  request.overrideMimeType('text/plain');
			}
		} else if (window.ActiveXObject) {
			request = new ActiveXObject('MSXML2.XMLHTTP.3.0');
		} else {
			throw 'XMLHttpRequest is disabled';
		}
		
		request.open('GET', url, false);
		request.send(null);
		
		if (request.readyState != 4) {
			throw error;
		}
		
		return request.responseText;
			return shaders;
	}
	
	
	/**
	 * clone object (Deprecated)
	 * @param {object} object you want to clone
	 */
	raptorjs.clone = function (obj) {
		var cloneObject = raptorjs.createObject(obj._className);
		for(var el in obj) {
			cloneObject[el] = obj[el];
		}
		return cloneObject;
	}
	
	/**
	 * start Render pipeline
	 */
	raptorjs.startRenderPipeline = function() {
		gl.enable(gl.DEPTH_TEST);
		gl.depthFunc(gl.LEQUAL);
		
		tick();
	}
	
	/**
	 * Tick
	 */
    function tick() {
        requestAnimFrame(tick);
		raptorjs.system.smoothTimeEvolution();
        raptorjs.renderCallback();
    }
	
	
	/**
	 * Benchmark a function
	 */
	function benchmarkFunction(funct, times) {
		var date1 = new Date(); 
		var milliseconds1 = date1.getTime(); 
		var j = 0;
		
		for (i = 0; i < times; i++) { 
			funct();
		}
		
		var date2 = new Date(); 
		var milliseconds2 = date2.getTime(); 

		var difference = milliseconds2 - milliseconds1; 
		console.log(difference, 'millisec'); //, funct
	}
	
	
	/*
	 *	Create error texture
	 */
	raptorjs.createErrorTexture = function() {
		var dataArray = [];
		var width = 512;
		var height = 512;
		
		for( var y = 0; y < height; y++ )
		{
			for( var x = 0; x < width; x++ )
			{
				dataArray.push( x / width );
				dataArray.push( y / width );

				dataArray.push(  x / x / width );
				dataArray.push(  y * x / width ); 
			}
		}
		
		var text = raptorjs.textureFromArray(dataArray, width, height, true);
		var sampler = raptorjs.createObject("sampler2D");
		sampler.texture = text;
	
		raptorjs.errorTexture = sampler;
	}