/*
 * Copyright 2012, Raptorcode Studios Inc, 
 * Author, Kaj Dijkstra.
 * All rights reserved.
 *
 */

 
/**
 * Entity object
**/
raptorjs.entity = function() {
	this._className = 'entity';
	this.subEntitys = [];
	
	this.name;
	this.mesh;
	this.drawType = gl.TRIANGLES;
	this.transform = raptorjs.createObject('transform');
	this.transform.entity = this;
	this.bone = false;
	
	this.children = [];
	this.parent = false;
	this.objectTransform;
	this.attenuation = 1.0;
	this.sourceRadius = 0.4;
	this.sourceLength = 0.2;
	
	this.attenuation = 1.;
	this.sourceRadius = 0.0;
	this.sourceLength = 0.0;
	
	this.type = "Actor";
};


raptorjs.entity.prototype.translateTo = function( x, y, z ) {
	console.log(raptorjs.entity.name,x, y, z);
	this.transform.world = raptorjs.matrix4.translate(raptorjs.matrix4.identity(), raptorjs.vector3(x,y,z) );
}

raptorjs.entity.prototype.addChild = function( entity ) {
	entity.parent = this;
	this.children.push(entity);
}

raptorjs.entity.prototype.getChildren = function() {
	return this.children;
}

raptorjs.entity.prototype.getChildByName = function( name ) {
	for(var c=0;c<this.childen.lenght; c++) {
		if(this.childen[c].name == name)
			return this.childen[c];
	}
}

raptorjs.entity.prototype.getParent = function() {
	return this.parent;
}

/**
 * add mesh to entity
 * @param {(meshObject)} mesh
**/
raptorjs.entity.prototype.addMesh = function(mesh) {
	this.mesh = mesh;
	
	var subMeshes = this.mesh.subMeshes;
	
	for(var c = 0;c<subMeshes.length; c++) {
		var subMesh = subMeshes[c];
	
		var newSubEntity = raptorjs.createObject("subEntity");
		newSubEntity.subMesh = subMesh;
		
		this.addSubEntity(newSubEntity);
	}
};


/**
 * add subEntity to entity
 * @param {(subEntityObject)} subEntity
**/
raptorjs.entity.prototype.addSubEntity = function(subEntity) {
	this.subEntitys.push(subEntity);
};


/**
 * get subentity from entity
**/
raptorjs.entity.prototype.getSubEntitys = function() {
	return this.subEntitys;
};


/**
 * update Uniforms
 * @param {(subEntityObject)} subEntity
**/
raptorjs.entity.prototype.updateUniforms = function() {
	var shader = this.shader;
	var transform = this.transform;
}


/**
 * get updated 4x4 world matrix
**/
raptorjs.entity.prototype.getUpdatedWorldMatrix = function() {
	var children = this.getChildOfNode(this, []).reverse();

	
	var transform =  raptorjs.matrix4.identity();
	
	for(var c = 0; c<children.length;c++) {
		var currentParent = children[c];
		transform = raptorjs.matrix4.mul(currentParent.transform.world, transform);
		
	}
	
	
	
	//transform = transform;//raptorjs.matrix4.scale(this.transform.world, [100, 100, 100]);
	
	return transform;
}


raptorjs.entity.prototype.getChildOfNode = function(node, children){
	children.push(node);
	
	if(node.parent)
		return this.getChildOfNode(node.parent, children);
	else
		return children;
} 