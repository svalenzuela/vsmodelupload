/*
 * Copyright 2013, Raptorcode Studios Inc, 
 * Author, Kaj Dijkstra.
 * All rights reserved.
 *
 */
 var global_material_id = 0;

/**
 * Material
**/
raptorjs.material = function () {
	this.name = "none";
	
	this.color;
	this.properties = [];
	
	this.textures = [];
	this.normals = [];
	this.displacementMaps = [];
	this.transparencyMaps = [];
	this.roughnessMaps = [];
	this.specularMaps = [];
	
	
	this.diffuseColor = [0, 0,0];
	
	this.shadingModelID = 0.0;
	this.useParallax = false;
	
	this.uvScale = 1;
	
	this.roughness = 0.4;
	//this.roughness = 0.75;
	this.metallic = 0.8;
	
	this.alpha = 1.0;
	this.specular = .2;
	
	this.id = global_material_id++;
}

raptorjs.material.prototype.setShadingModelID  = function( id ) {
	
	this.shadingModelID = id;
}
raptorjs.material.prototype.getShadingModelID  = function( id ) {
	return this.shadingModelID;
}
raptorjs.material.prototype.addProperty  = function(key, value) {
	this.properties.push([key, value]);
}
/**
 * add texture to material
 * @param {(texture)} texture
**/
raptorjs.material.prototype.addTexture = function(texture) {
	this.textures = [];
	this.textures.push(texture);
}


/**
 * add normal map to material
 * @param {(texture)} texture
**/
raptorjs.material.prototype.addNormal = function(texture) {
	this.normals = [];
	this.normals.push(texture);
}

/**
 * add normal map to material
 * @param {(texture)} texture
**/
raptorjs.material.prototype.addRoughness = function(texture) {
	this.roughnessMaps.push(texture);
}


/**
 * add transparency map to material
 * @param {(texture)} transparentyMap
**/
raptorjs.material.prototype.addTransparentyMap = function(texture) {
	this.transparencyMaps.push(texture);
}


/**
 * add displacement map to material
 * @param {(texture)} heightmap
**/
raptorjs.material.prototype.addDisplacement = function( heightmap ) {
	this.useParallax = true;
	this.displacementMaps.push(heightmap);
}


/**
 * add specular map to material
 * @param {(texture)} specularMap
**/
raptorjs.material.prototype.addSpecularMap = function( specularMap ) {
	this.specularMaps.push(specularMap);
}
