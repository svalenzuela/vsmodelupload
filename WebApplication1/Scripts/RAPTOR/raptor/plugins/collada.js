
	raptorjs.collada = function(){
	
	}
	
	raptorjs.collada.prototype.parse = function( data ) {
	
		var xmlDoc = parseXml(data).documentElement;
		var effects = xmlDoc.getElementsByTagName('library_effects')[0];
		var materials = xmlDoc.getElementsByTagName('library_materials')[0];
		var geometries = xmlDoc.getElementsByTagName('library_geometries')[0];
		var lights = xmlDoc.getElementsByTagName('library_lights')[0];
		var images = xmlDoc.getElementsByTagName('library_images')[0];
		var visuals = xmlDoc.getElementsByTagName('library_visuals')[0];

		var verticeArray = [];
		var normalArray = [];
		var uvArray = [];
		var indexArray = [];
		
		var vertexIndices = [];
		
		var collada = raptorjs.createObject("collada");
		console.log(xmlDoc);
		collada.parse(xmlDoc);

		for(var g = 0; g<30;g++) {
		
			var child = geometries.childNodes[(g*2)+1]; // even keys (c*2)+1

			var mesh = child.childNodes[1];
			var source = child.getElementsByTagName("source");
			
			var positionSource;
			var coordSource;
			var normalSource;
			
			for(var c = 0; c<source.length; c++) {
				var currentSource = source[c];

				var id = currentSource.getAttribute('id');
				
				if(id.endsWith('positions')) {
					positionSource = currentSource;
				}	
				
				if(id.endsWith('normals')) {
					normalSource = currentSource;
				}
				
				if(id.endsWith('map1')) {
					coordSource = currentSource;
				}
			}
			
			var triangle = child.getElementsByTagName("triangles");
			var inputs = triangle[0].getElementsByTagName('input');
			
			
			var positions	    = positionSource.getElementsByTagName('float_array')[0].childNodes[0].data.split(' ').map(parseFloat);
			var positionStride  = parseInt( positionSource.getElementsByTagName('accessor')[0].getAttribute('stride') );
			var positionOffset  = parseInt( findSemantic(inputs, 'VERTEX').getAttribute('offset') );
		
			var coords       = coordSource.getElementsByTagName('float_array')[0].childNodes[0].data.split(' ').map(parseFloat);
			var coordStride  = parseInt( coordSource.getElementsByTagName('accessor')[0].getAttribute('stride') );
			var coordOffset  = parseInt( findSemantic(inputs, 'TEXCOORD').getAttribute('offset') );
		
			var normals       = normalSource.getElementsByTagName('float_array')[0].childNodes[0].data.split(' ').map(parseFloat);
			var normalsStride  = parseInt( normalSource.getElementsByTagName('accessor')[0].getAttribute('stride') );
			var normalsOffset  = parseInt( findSemantic(inputs, 'NORMAL').getAttribute('offset') );
			
			console.log(positions, positionStride, positionOffset);
			console.log(coords, coordStride, coordOffset);
			console.log(normals, normalsStride, normalsOffset);
			
			
			// unpack indices
			var packedIndices =  triangle[0].getElementsByTagName('p')[0].childNodes[0].data.split(' ').map(parseFloat);//.data.split(' ')
			
			var vertexIndices  = [];
			var normalIndices  = [];
			var coordIndices   = [];
			var tangentIndices = [];
			
			for(var c = 0; c<packedIndices.length;c++) {
				var stride = (c % 4);
				
				var value = packedIndices[ c ];
				
				if(stride == positionOffset) {
					vertexIndices.push(value);
				}
				
				if(stride == coordOffset) {
					coordIndices.push(value);
				}
				
				if(stride == normalsOffset) {
					normalIndices.push(value);
				}
			}

			
			var texcoordArray = [];
			var positionArray = [];
			var normalArray = [];
			
			for(var b = 0; b<vertexIndices.length; b++) {

				texcoordArray[b] = coords[coordIndices[b]];

				
			}
			
		//	for(var b = 0; b<vertexIndices.length * 2; b++) {
		//		texcoordArray[vertexIndices[b]] = texcoordArray[b];
		//	}
			// vertexIndices = [2, 5, 7]
			// normalIndices = [10, 100, 300]
			// 					2 -> 10, 5 -> 100
			//					v    n,  v    n
			var newVertices = [];
			
			for(var b = 0; b<vertexIndices.length * 3; b++) {
				positionArray[b] = positions[vertexIndices[b]];
				
				newVertices[vertexIndices[b]] = b;
				
			}
			


			for(var b = 0; b<vertexIndices.length; b++) {
				var vertexIndex = b*3;
				
				normalArray[vertexIndex] = normals[normalIndices[vertexIndex]];
				normalArray[vertexIndex+1] = normals[normalIndices[vertexIndex]+1];
				normalArray[vertexIndex+2] = normals[normalIndices[vertexIndex]+2];
			}
			
		
			
			
			console.log('normal', normalArray, 'vert', vertexIndices, 'uv',coordIndices, 'vertexIndices',vertexIndices);
		

		
			var material = raptorjs.createObject("material");
			material.addTexture(normalSampler);

			var mesh = raptorjs.createObject("mesh");
			mesh.createMeshFromArrays(newVertices, positions, normalArray, texcoordArray);
			mesh.addMaterial(material);

			var entity = raptorjs.createObject("entity");
			entity.addMesh(mesh);
			entity.transform.rotateX(-90);
		
			raptorjs.scene.addEntity( entity );
			
			console.log(texcoordArray, positionArray, vertexIndices);
		
			//console.log(vertices, normals, coords, vertexIndices);
			//console.log(unsortedVertexIndices.length, normals.length, coords.length, vertexIndices.length);
			//verticeArray = verticeArray.concat(vertices);
			//indexArray = indexArray.concat(indices);
			//uvArray = uvArray.concat(vertices);
			//verticeArray = verticeArray.concat(vertices);
			
		}
		

	}