<?php

if ($handle = opendir('media/textures/sponza/')) {

	$all = [];
	
    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {

		array_push($all, $entry);
    }

	array_shift($all);
	array_shift($all);

	array_pop($all);
	array_pop($all);
	
	echo json_encode($all);

    closedir($handle);
}
?>