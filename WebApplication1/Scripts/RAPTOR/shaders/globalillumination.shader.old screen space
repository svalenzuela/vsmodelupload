/**
 * Raptor Engine - Core
 * Copyright (c) 2010 RAPTORCODE STUDIOS
 * All rights reserved.
 */
  
/**
 * Author: Kaj Dijksta
 */
 
    attribute vec3 position;
    attribute vec2 uv;
	
    uniform mat4 viewProjection;

    varying vec2 v_uv;

    void main(void) {
        v_uv = uv;
		gl_Position = viewProjection * vec4(position, 1.0);
    }
	
	// #raptorEngine - Split
	
	precision highp float;
	
	varying vec2 v_uv;
	
	uniform sampler2D infoSampler;
	uniform sampler2D albedoSampler;
	uniform sampler2D diffuseAccSampler;
	uniform sampler2D randomSampler;

	uniform float screenWidth;
	uniform float screenHeight;
	
	uniform float far;
	
	uniform vec3 frustumWorldCorners[8];
	
	uniform vec3 cameraPosition;
	uniform float test;
	
	struct fragmentPass {
		highp vec2 uv;
		
		mediump vec3 diffuse;
		
		mediump vec3 normal;
		mediump vec3 normalW;
		mediump vec3 positionWorld;
		
		mediump float ambiantOcclusion;
		mediump float shadowOcclusion;
		
		highp float far;
		highp float depth;
		highp float depthNorm;
	};
	
	mediump vec3 decodeNormal( mediump vec2 enc ) {
		mediump vec2 fenc = enc * 4.0 - 2.0;
		mediump float f = dot(fenc, fenc);
		mediump float g = sqrt(1.0 - f / 4.0);
		mediump vec3 n;
		n.xy = fenc * g;
		n.z = 1.0 - f / 2.0;
		return n;
	}
	
	mediump vec4 DecodeColor(mediump vec4 color) {
	  color.rgb *= vec3(color.a * 64.0);
	  return color;
	}
	
	mediump vec4 SSGI( vec3 normal, vec2 uv, float depth ) {
	
		const mediump vec4 SSAO_params = vec4( 1.0, 0.075, 1.0, 2.0 );
		
		const mediump int samplesNum = 4; // QUALITY_HIGH;

		const mediump float radius = 0.3; // 0.2

		mediump float effectAmount = SSAO_params.x / float(samplesNum);
		
		vec3 kernel[32];
		
		kernel[0] = vec3(-0.556641,-0.037109,-0.654297); 
		kernel[1] = vec3(0.173828,0.111328,0.064453); 
		kernel[2] = vec3(0.001953,0.082031,-0.060547); 
		kernel[3] = vec3(0.220703,-0.359375,-0.062500);
		kernel[4] = vec3(0.242188,0.126953,-0.250000); 
		kernel[5] = vec3(0.070313,-0.025391,0.148438); 
		kernel[6] = vec3(-0.078125,0.013672,-0.314453);
		kernel[7] = vec3(0.117188,-0.140625,-0.199219);
		kernel[8] = vec3(-0.251953,-0.558594,0.082031); 
		kernel[9] = vec3(0.308594,0.193359,0.324219); 
		kernel[10] = vec3(0.173828,-0.140625,0.031250);
		kernel[11] = vec3(0.179688,-0.044922,0.046875); 
		kernel[12] = vec3(-0.146484,-0.201172,-0.029297);
		kernel[13] = vec3(-0.300781,0.234375,0.539063); 
		kernel[14] = vec3(0.228516,0.154297,-0.119141); 
		kernel[15] = vec3(-0.119141,-0.003906,-0.066406); 
		kernel[16] = vec3(-0.218750,0.214844,-0.250000);
		// kernel[17] = vec3(0.113281,-0.091797,0.212891); 
		// kernel[18] = vec3(0.105469,-0.039063,-0.019531);
		// kernel[19] = vec3(-0.705078,-0.060547,0.023438); 
		// kernel[20] = vec3(0.021484,0.326172,0.115234); 
		// kernel[21] = vec3(0.353516,0.208984,-0.294922);
		// kernel[22] = vec3(-0.029297,-0.259766,0.089844);
		// kernel[23] = vec3(-0.240234,0.146484,-0.068359);
		// kernel[24] = vec3(-0.296875,0.410156,-0.291016);
		// kernel[25] = vec3(0.078125,0.113281,-0.126953); 
		// kernel[26] = vec3(-0.152344,-0.019531,0.142578);
		// kernel[27] = vec3(-0.214844,-0.175781,0.191406);
		// kernel[28] = vec3(0.134766,0.414063,-0.707031); 
		// kernel[29] = vec3(0.291016,-0.833984,-0.183594);
		// kernel[30] = vec3(-0.058594,-0.111328,0.457031);
		// kernel[31] = vec3(-0.115234,-0.287109,-0.259766);  


		mediump vec3 randomSample = texture2D(randomSampler, vec2(screenWidth, screenHeight) * uv / 4.0).xyz  * 2.0 - 1.0;
		mediump vec3 colorBleeding = vec3(0.0);

		for (int i = 0; i < samplesNum; i++)
		{
			mediump vec3 sample;

			mediump float sampleDepth, tapDepth;
			mediump float alpha, gamma;

			/////////////////////////
			// Construct the sample
			sample = reflect(kernel[i] * radius, randomSample);
			sample = sample * (dot(sample, normal) < 0.0 ? -1.:1.);

			/////////////////////////
			// Find the offset
			gamma = 1. / (1.0 + sample.z);
			mediump vec2 centerPos = (uv * 2.0) - vec2(1.0, 1.0);
			mediump vec2 samplePos = (centerPos + sample.xy) * (gamma * 0.5) + 0.5;

			/////////////////////////
			// Get tap data
			mediump vec3 tapNormal, tapAlbedo, tapRadiance;

			tapDepth  = texture2D( infoSampler, (samplePos)).z ; 
			alpha = gamma * tapDepth;

			tapNormal =  decodeNormal(texture2D( infoSampler, samplePos ).xy);
			// tapNormal = normalize(mul(SSAO_CameraMatrix, tapNormal));

			tapAlbedo = texture2D(albedoSampler, samplePos).rgb;  
			tapRadiance = (texture2D(diffuseAccSampler, samplePos)).rgb; 

			// Compute the direction vector between the point and the bleeder
			mediump vec3 D = vec3(centerPos, 1.0) * (alpha - depth) + sample * alpha;

			const mediump float minDist = 0.0005;
			mediump float r = max(minDist, length(D));

			D = normalize(D);

			// Compute attenuation
			mediump float atten = 
				//pow(minDist / r, 2.)
				//min(depth * pow(0.05 / r, 2.0), 1.)
				min(pow(0.05 * depth / r, 2.0), 1.)
				//1
				;

			mediump float factor = 400.
				// Visibility
				* (((depth * (1. + sample.z) - tapDepth) > 0.0 )? 1.0 : 0.4 ) //> 0.
				// Reflector lambertian term
				* max(0., -dot(D, tapNormal))
				// Incident radiance projection term
				* dot(D, normal) 
				// Attenuation term
				* atten
				;

			mediump vec3 radiance = tapAlbedo * factor * tapRadiance;
			colorBleeding += radiance;
		}

		colorBleeding *= effectAmount;

		return vec4(max(vec3(0.0), colorBleeding), 1.0);
	}
	
	vec3 constructPositionWorld( vec2 uv, float depth ) {
		return mix(
					mix(
						mix(frustumWorldCorners[0], frustumWorldCorners[1], uv.x), 
						mix(frustumWorldCorners[3], frustumWorldCorners[2], uv.x), 
						uv.y ), 		
					mix( 
						
						mix(frustumWorldCorners[7], frustumWorldCorners[6], uv.x), 
						mix(frustumWorldCorners[4], frustumWorldCorners[5], uv.x), 
						uv.y ), 
					depth ) + vec3( cameraPosition.y,cameraPosition.z, test);
	}

	
	void main() {
	
		vec4 passInfo = texture2D( infoSampler, v_uv.xy );
	
		fragmentPass pass;
		
		pass.uv = v_uv;
		pass.normalW = decodeNormal( passInfo.xy );
		pass.normal = pass.normalW;
		pass.depth = passInfo.z;
		pass.depthNorm = pass.depth / far;

	gl_FragColor = texture2D(diffuseAccSampler, v_uv);
			// 	gl_FragColor = SSGI( pass.normal, pass.uv, pass.depth ) ;
	}

	