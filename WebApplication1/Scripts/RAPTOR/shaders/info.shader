/**
 * 	Raptor Engine - Core
 * 	Copyright (c) 2010 RAPTORCODE
 * 	All rights reserved.
 * 
 */
 
 /**
 * 	Author: Kaj Dijksta
 */
 
	#ifndef NORMAL_MAP
		#define NORMAL_MAP 0
	#endif
	
    attribute vec3 position;
	attribute vec3 normal;
	attribute vec3 tangent;
	attribute vec3 binormal;

	attribute vec2 uv;
	
    uniform mat4 view;
    uniform mat4 projection;

	uniform mat4 worldViewProjection;
	uniform mat4 world;

	uniform sampler2D heightmap;
	
	varying vec2 v_uv;
	
    varying vec4 v_normal;
	varying vec4 v_tangent;
	varying vec4 v_binormal;
	
	varying vec4 v_position;
	varying vec4 v_worldposition;
	varying float v_linear_depth;

    void main(void) {

		v_uv = uv;
		
		v_normal  =  normalize(world * vec4(normal, 0.0));
		v_tangent =   normalize(world * vec4(tangent, 0.0));
		v_binormal =   normalize(world * vec4(binormal, 0.0));
		
		// v_tangent = normalize(cross(v_normal, v_tangent));
		 
		v_worldposition = world * vec4(position, 1.0);
		 
		gl_Position = worldViewProjection * vec4(position, 1.0);
    }
	
	// #raptorEngine - Split


	
	
	#extension GL_EXT_draw_buffers : require

	#define PI 3.14159265358979323846
	    precision highp float;
	
	

	#ifndef NORMAL_MAP
		#define NORMAL_MAP 0
	#endif

	uniform float far;
	uniform vec3 cameraPosition;

	uniform float v_fresnel;
	uniform float v_roughness;
	
	uniform sampler2D normalSampler;
	uniform sampler2D texture;
	
	varying vec4 v_position;	
	varying vec4 v_worldposition;
	
	varying vec2 v_uv;
	varying vec4 v_normal;
	
	varying vec4 v_tangent;
	varying vec4 v_binormal;
	
	varying float v_linear_depth;
	
	uniform mat4 view;

	
	float blinnPhongSpecular(vec3 lightDirection, vec3 viewDirection, vec3 surfaceNormal, float shininess) {

	  //Calculate Blinn-Phong power
	  vec3 H = normalize(viewDirection + lightDirection);
	  return pow(max(0.0, dot(surfaceNormal, H)), shininess);
	}
	
	float fresnel(vec3 direction, vec3 normal, bool invert) {
		vec3 nDirection = normalize( direction );
		vec3 nNormal = normalize( normal );
		vec3 halfDirection = normalize( nNormal + nDirection );
		
		float cosine = dot( halfDirection, nDirection );
		float product = max( cosine, 0.0 );
		float factor = invert ? 1.0 - pow( product, 5.0 ) : pow( product, 5.0 );
		
		return factor;
	}
	
	float beckmannDistribution(float x, float roughness) {
	  float NdotH = max(x, 1e-4);
	  float cos2Alpha = NdotH * NdotH;
	  float tan2Alpha = (cos2Alpha - 1.0) / cos2Alpha;
	  float roughness2 = roughness * roughness;
	  float denom = PI * roughness2 * cos2Alpha * cos2Alpha;
	  return exp(tan2Alpha / roughness2) / denom;
	}

	float beckmannSpecular(vec3 lightDirection, vec3 viewDirection,vec3 surfaceNormal,float roughness) {
	
	  return beckmannDistribution(dot(surfaceNormal, normalize(lightDirection + viewDirection)), roughness);
	  
	}
	
	
	
	
float cookTorrance(vec3 lightDirection, vec3 viewDirection, vec3 surfaceNormal,float roughness, float fresnel) {

  float VdotN = max(dot(viewDirection, surfaceNormal), 0.0);
  float LdotN = max(dot(lightDirection, surfaceNormal), 0.0);

  //Half angle vector
  vec3 H = normalize(lightDirection + viewDirection);

  //Geometric term
  float NdotH = max(dot(surfaceNormal, H), 0.0);
  float VdotH = max(dot(viewDirection, H), 1e-6);
  float LdotH = max(dot(lightDirection, H), 1e-6);
  float G1 = (2.0 * NdotH * VdotN) / VdotH;
  float G2 = (2.0 * NdotH * LdotN) / LdotH;
  float G = min(1.0, min(G1, G2));

  //Distribution term
  float D = beckmannDistribution(NdotH, roughness);

  //Fresnel term
  float F = pow(1.0 - VdotN, fresnel);

  //Multiply terms and done
  return G * F * D / max(PI * VdotN, 1e-6);
}


vec3 beckmannSpecular2(vec3 varNormal, vec3 lightDirection, vec3 varEyeDir){
	float roughnessValue = 0.3; // 0 : smooth, 1: rough
    float F0 = 0.8; // fresnel reflectance at normal incidence
    float k = 0.2; // fraction of diffuse reflection (specular reflection = 1 - k)
    vec3 lightColor = vec3(0.9, 0.9, 0.9);
    
    // interpolating normals will change the length of the normal, so renormalize the normal.
    vec3 normal = normalize(varNormal);
    
    // do the lighting calculation for each fragment.
    float NdotL = max(dot(normal, lightDirection), 0.0);
    
    float specular = 0.0;
    if(NdotL > 0.0) {
	
        vec3 eyeDir = normalize(varEyeDir);

        // calculate intermediary values
        vec3 halfVector = normalize(lightDirection + eyeDir);
        float NdotH = max(dot(normal, halfVector), 0.0); 
        float NdotV = max(dot(normal, eyeDir), 0.0); // note: this could also be NdotL, which is the same value
        float VdotH = max(dot(eyeDir, halfVector), 0.0);
        float mSquared = roughnessValue * roughnessValue;
        
        // geometric attenuation
        float NH2 = 2.0 * NdotH;
        float g1 = (NH2 * NdotV) / VdotH;
        float g2 = (NH2 * NdotL) / VdotH;
        float geoAtt = min(1.0, min(g1, g2));
     
        // roughness (or: microfacet distribution function)
        // beckmann distribution function
        float r1 = 1.0 / ( 4.0 * mSquared * pow(NdotH, 4.0));
        float r2 = (NdotH * NdotH - 1.0) / (mSquared * NdotH * NdotH);
        float roughness = r1 * exp(r2);
        
        // fresnel
        // Schlick approximation
        float fresnel = pow(1.0 - VdotH, 5.0);
        fresnel *= (1.0 - F0);
        fresnel += F0;
        
        specular = (fresnel * geoAtt * roughness) / (NdotV * NdotL * 3.14);
    }
    
    vec3 finalValue = lightColor * NdotL * (k + specular * (1.0 - k));
	
	return finalValue;
}


	void main() {

		
	

		#if NORMAL_MAP == 1						 
			//mat3 tangentToWS = mat3( v_tangent.xyz,
			//						 v_binormal.xyz,
			//						 v_normal.xyz );
									 
			vec4 normalMap = texture2D(normalSampler, v_uv) * 2.0 - 1.0;
			//vec3 normal =   normalMap.xyz * tangentToWS; 
			
			vec3 normal = normalize((v_tangent.xyz * normalMap.x) + (v_binormal.xyz * normalMap.y) + (v_normal.xyz * normalMap.z));
		#else
			vec3 normal =   v_normal.xyz; 	
		#endif
		
		//vec3 lightDirection = vec3(1.0, 1.0, 1.0);
		vec3 lightPosition = vec3(40.0);
		//vec3 viewDirection = -normalize(cameraPosition);
		//float shininess = 20.0;
		
		//float power = blinnPhongSpecular(lightDirection, viewDirection, normal, shininess);
		//float fresh = fresnel(lightDirection, normal, false);
		
		
		
  vec3 viewDirection = normalize(cameraPosition - v_position.xyz);
  vec3 lightDirection = normalize(lightPosition - v_position.xyz);

  //Surface properties
  normal = normalize(normal);

  //Compute specular power
  float power = cookTorrance(lightDirection, viewDirection, normal,  .5, .2);


		
	float beckmann = beckmannSpecular(lightDirection, lightDirection, normal, .6);
	//	vec3 beckmann = beckmannSpecular2(normal, lightDirection, viewDirection);
		
		
		
			float f = fresnel(cameraPosition, normal, false);
			float cook = cookTorrance(lightDirection, viewDirection, normal, .3, f);
		
		gl_FragData[0]  = texture2D(texture, v_uv);//vec4(power);//texture2D(texture, v_uv);// texture
		
		gl_FragData[1] = vec4(normal.xyz,1.0) + texture2D(texture, v_uv);
		gl_FragData[2] = v_worldposition;
		
		
	}
	