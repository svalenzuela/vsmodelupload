<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<script type="text/javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
		<script src='/Scripts/RAPTOR/spectrum.js'></script>
		
		<link rel='stylesheet' href='/Scripts/RAPTOR/spectrum.css' />

		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/downloadify.min.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/swfobject.js"></script>

		<!-- core -->
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/utils.js"></script>
		<script> var basepath = "/Scripts/RAPTOR/";</script>
		<script type="text/javascript" src="/Scripts/functions.js" ></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/raptor.js"></script>

		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/math.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/resourceManager.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/eventManager.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/transform.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/shader.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/camera.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/framebuffer.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/samplerCube.js"></script>
		
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/mesh.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/subMesh.js"></script>
		
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/entity.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/subEntity.js"></script>
		
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/primitives.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/player.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/renderSystem.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/sceneNode.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/programInfo.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/skeleton.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/bone.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/sampler2d.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/texture.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/boundingBox.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/material.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/raptor/core/assimp.js"></script>


		<link rel='stylesheet' href='/Scripts/RAPTOR/third party/js/jqwidgets/styles/jqx.base.css' />
		<link rel='stylesheet' href='/Scripts/RAPTOR/third party/js/jqwidgets/styles/jqx.dark.css' />
		
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxcore.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxribbon.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxwindow.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxlayout.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxdockinglayout.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxtree.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxexpander.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxlistbox.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxbuttons.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxscrollbar.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxdropdownlist.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxcheckbox.js"></script>
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxslider.js"></script>
		
		<script type="text/javascript" src="/Scripts/RAPTOR/third party/js/jqwidgets/jqxmenu.js"></script>
		
		
		<link rel="stylesheet" media="screen" type="text/css" href="/Scripts/RAPTOR/style.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="/Scripts/Styles/style.css" />

	</head>

	<body style="border: none;" onload="if (typeof raptorjs != 'undefined') {   raptorjs.initialize(); }"><!---->
	
			<div id='jqxMenu' style='background: black; margin-left: 5px;'>
                <ul>
                    <li><a href="#Home">File</a>
					
						<ul style='width: 250px;'>
               
							<li>
								<input type="file" id="UploadModel" />
							</li>
 
                        </ul>
						
					</li>
                    <li>Solutions
                        <ul style='width: 250px;'>
                            <li><a href="#Education">Education</a></li>
                            <li><a href="#Financial">Financial services</a></li>
                            <li><a href="#Government">Government</a></li>
                            <li><a href="#Manufacturing">Manufacturing</a></li>
                            <li type='separator'></li>
                            <li>Software Solutions
                                <ul style='width: 220px;'>
                                    <li><a href="#ConsumerPhoto">Consumer photo and video</a></li>
                                    <li><a href="#Mobile">Mobile</a></li>
                                    <li><a href="#RIA">Rich Internet applications</a></li>
                                    <li><a href="#TechnicalCommunication">Technical communication</a></li>
                                    <li><a href="#Training">Training and eLearning</a></li>
                                    <li><a href="#WebConferencing">Web conferencing</a></li>
                                </ul>
                            </li>
                            <li><a href="#">All industries and solutions</a></li>
                        </ul>
                    </li>
                    <li>Products
                        <ul>
                            <li><a href="#PCProducts">PC products</a></li>
                            <li><a href="#MobileProducts">Mobile products</a></li>
                            <li><a href="#AllProducts">All products</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
	
	
	<div id="jqxDockingLayout">

        <!--documentGroup-->
        <div data-container="Document1Panel">
           <canvas id="raptorEngine" style="border: none; margin:0; padding:0; width: 100%; height: 100%;"></canvas>
			
		</div>
        <div data-container="Document2Panel">
            Document 2 content</div>
        <!--bottom tabbedGroup-->
        <div data-container="ErrorListPanel">
            List of errors
			
		</div>
        <!--right tabbedGroup-->
        <div data-container="SolutionExplorerPanel">
            <div id="solutionExplorerTree" style="border: none;">
            </div>
        </div>
        <div data-container="DetailsPanel" >
			<div id="details">
			</div>
			
			
		</div>
        <!--floatGroup-->
        <div data-container="OutputPanel" id="canvas">
            
         
        </div>
			</div>
		
		
		<!--
		
		<div id="control_pannel" style="">
			<div class="title">
				Spotlight
			</div>
			<table>
				<tr>
					<td>lightPosition</td>
					<td>
						<input id="position_x" type="number"  min="0" max="100" step="0.1" value="0" />
						<input id="position_y" type="number" min="0" max="100" step="0.1" value="0.1" />
						<input id="position_z" type="number" min="0" max="100" step="0.1" value="0" /></td>
				</tr>
				
				<tr>
					<td>Spot angles</td>
					<td>
						<input id="spotAngleX" type="number"  min="0" max="100" step="0.1" value="30" />
						<input id="spotAngleY" type="number" min="0" max="100" step="0.1" value="0.1" />
					</td>
				</tr>
				
				<tr>
					<td>Light Direction</td>
					<td>
						<input id="LightDirectionX" type="number"  min="-300" max="300" step=".01" value="0.15" />
						<input id="LightDirectionY" type="number" min="-300" max="300" step=".01" value="1.0" />
						<input id="LightDirectionZ" type="number" min="-300" max="300" step="2" value="5.5" />
					</td>
				</tr>
				
				<tr>
					<td>light color</td>
					<td>
						<input type='text' id="lightColor" />
					</td>
				</tr>
				<tr>
					<td>SSAO</td>
					<td>
						<input id="SSAOX" type="number"  min="-300" max="300" step="0.03" value="0.15" />
						<input id="SSAOY" type="number" min="-300" max="300" step="0.005" value="0.45" />
						<input id="SSAOZ" type="number" min="-300" max="300" step="0.1" value="0.5" />
						<input id="SSAOW" type="number" min="-300" max="300" step="0.1" value="1.2" />
					</td>
				</tr>
				
				
			</table>

		</div>

		-->
		
		
	</body>

