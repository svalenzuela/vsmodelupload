

    attribute vec2 uv;
    attribute vec3 position;
	
    uniform mat4 viewProjection;

    varying vec2 v_uv;

    void main(void) {
        v_uv = uv;
		
		gl_Position = viewProjection *  vec4(position, 1.0);
    }
	
	// #raptorEngine - Split
	
	precision highp float;

	#define PI 3.14159265358979323846
	
    varying vec2 v_uv;

	uniform sampler2D sampler;
	uniform sampler2D diffuseSampler;
	uniform sampler2D normalSampler;
	uniform sampler2D infoSampler;
	uniform sampler2D randomSampler;
	uniform sampler2D ambientOcclusionSampler;
	
	uniform samplerCube reflectionSampler;
	
	uniform vec3 cameraPosition;
	uniform vec3 lightPosition;
	uniform vec3 LightDirection;	
	uniform vec3 LightColor;	
	uniform vec2 SpotAngles;
	
	uniform float far;

	uniform mat4 InvProjection;

	uniform float attenuation; 
	uniform float SourceRadius;
	uniform float SourceLength;
	uniform float metallic;
	
	float Square(float f){
		return (f * f);
	}
	
	vec3 Square(vec3 f){
		return (f * f);
	}
	
	float rcp(float a) {
		return 1.0 / a;
	}
	
	vec3 Diffuse_Lambert( vec3 DiffuseColor )
	{
		return DiffuseColor * (1.0 / PI);
	}

	float D_GGX( float Roughness, float NoH )
	{
		float a = Roughness * Roughness;
		float a2 = a * a;
		float d = ( NoH * a2 - NoH ) * NoH + 1.0;	// 2 mad
		return a2 / ( PI*d*d );					// 4 mul, 1 rcp
	}

	vec3 F_Schlick( vec3 SpecularColor, float VoH )
	{

		return SpecularColor + ( clamp( 50.0 * SpecularColor.g, 0.0, 1.0 ) - SpecularColor ) * exp2( (-5.55473 * VoH - 6.98316) * VoH );
	}

	float Luminance( vec3 LinearColor )
	{
		return dot( LinearColor, vec3( 0.3, 0.59, 0.11 ) );
	}

	
	float BiasedNDotL(float NDotLWithoutSaturate )
	{
		return clamp(NDotLWithoutSaturate * 1.08 - 0.08, 0.0, 1.0);
	}	
	
	vec3 Diffuse_Burley( vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH )
	{
		float FD90 = 0.5 + 2.0 * VoH * VoH * Roughness;
		float FdV = 1.0 + (FD90 - 1.0) * exp2( (-5.55473 * NoV - 6.98316) * NoV );
		float FdL = 1.0 + (FD90 - 1.0) * exp2( (-5.55473 * NoL - 6.98316) * NoL );
		return DiffuseColor / PI * FdV * FdL;
	}
	
	vec3 Diffuse( vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH )
	{
		//#if   PHYSICAL_DIFFUSE == 0
		//		return Diffuse_Lambert( DiffuseColor );
		//#elif PHYSICAL_DIFFUSE == 1
				return Diffuse_Burley( DiffuseColor, Roughness, NoV, NoL, VoH );
		//#elif PHYSICAL_DIFFUSE == 2
		//		return Diffuse_OrenNayar( DiffuseColor, Roughness, NoV, NoL, VoH );
		//#endif
	}
	
	vec3 PointLightDiffuse(float _Roughness, vec3 albedo, vec3 VectorToLight, vec3 V, vec3 N )
	{
		vec3 L = VectorToLight;

		vec3 H = normalize(V + L);
		float NoL = clamp( dot(N, L), 0.0, 1.0 );
		float NoV = clamp( dot(N, V), 0.0, 1.0 );
		float VoH = clamp( dot(V, H), 0.0, 1.0 );

		return Diffuse( albedo, _Roughness, NoV, NoL, VoH );
	}
	
	float ClampedPow(float X,float Y)
	{
			return pow( max( abs(X), 0.000001), Y );
	}
	
	float PhongShadingPow(float X, float Y)
	{

		return ClampedPow(X, Y);
	}

	float D_Blinn( float Roughness, float NoH )
	{
		float m = Roughness * Roughness;
		float m2 = m * m;
		float n = 2.0 / m2 - 2.0;
		return (n+2.0) / (2.0*PI) * PhongShadingPow( NoH, n );		// 1 mad, 1 exp, 1 mul, 1 log
	}


		float D_Beckmann( float Roughness, float NoH )
		{
			float m = Roughness * Roughness;
			float m2 = m * m;
			float NoH2 = NoH * NoH;
			return exp( (NoH2 - 1.0) / (m2 * NoH2) ) / ( PI * m2 * NoH2 * NoH2 );
		}
	float Distribution( float Roughness, float NoH )
	{
	//#if   PHYSICAL_SPEC_D == 0
	//	return D_Blinn( Roughness, NoH );
	//#elif PHYSICAL_SPEC_D == 1
	//	return D_Beckmann( Roughness, NoH );
	//#elif PHYSICAL_SPEC_D == 2
		return D_GGX( Roughness, NoH );
	//#endif
	}

	float Vis_Schlick( float Roughness, float NoV, float NoL )
	{
			float k = Square( Roughness ) * 0.5;
			float Vis_SchlickV = NoV * (1. - k) + k;
			float Vis_SchlickL = NoL * (1. - k) + k;
			return 0.25 / ( Vis_SchlickV * Vis_SchlickL );
	}
		float Vis_Kelemen( vec3 L, vec3 V )
		{
			return 1. / ( 2. + 2. * dot(L, V) );
		}
	float GeometricVisibility( float Roughness, float NoV, float NoL, float VoH, vec3 L, vec3 V )
	{
   // #if   PHYSICAL_SPEC_G == 0
	//        return Vis_Implicit();
	//#elif PHYSICAL_SPEC_G == 1
   //         return Vis_Neumann( NoV, NoL );
   // #elif PHYSICAL_SPEC_G == 2
           return Vis_Kelemen( L, V );
	//#elif PHYSICAL_SPEC_G == 3
	// 		return Vis_Schlick( Roughness, NoV, NoL );
   // #elif PHYSICAL_SPEC_G == 4
	//        return Vis_Smith( Roughness, NoV, NoL );
	//#endif
	}
	
	vec3 F_Fresnel( vec3 SpecularColor, float VoH )
	{
			vec3 SpecularColorSqrt = sqrt( clamp( vec3(0.0, 0.0, 0.0), vec3(0.99, 0.99, 0.99), SpecularColor ) );
			vec3 n = ( 1.0 + SpecularColorSqrt ) / ( 1.0 - SpecularColorSqrt );
			vec3 g = sqrt( n*n + VoH*VoH - 1.0 );
			return 0.5 * Square( (g - VoH) / (g + VoH) ) * ( 1.0 + Square( ((g+VoH)*VoH - 1.0) / ((g-VoH)*VoH + 1.0) ) );
	}

	vec3 Fresnel( vec3 SpecularColor, float VoH )
	{
	//#if   PHYSICAL_SPEC_F == 0
	//		return F_None( SpecularColor );
	//#elif PHYSICAL_SPEC_F == 1
			return F_Schlick( SpecularColor, VoH );
	//#elif PHYSICAL_SPEC_F == 2
		//	return F_Fresnel( SpecularColor, VoH );
	//#endif
	}

   vec3 PointLightSpecular(vec3 specular, float roughness, vec3 VectorToLight, vec3 V, vec3 N)
	{
			float Energy = 1.0;
			
			float a = roughness * roughness;

			vec3 R = reflect( -V, N );
			
			float RLengthL = inversesqrt( dot( VectorToLight, VectorToLight ) );
		   
			if( SourceLength > 0.0 )
			{
					float LineAngle = clamp( SourceLength * RLengthL, 0.0, 1.0 );
					Energy *= a / clamp( a + 0.5 * LineAngle, 0.0, 1.0 );

					// Closest point on line segment to ray
					vec3 Ld = VectorToLight * SourceLength;
					vec3 L0 = VectorToLight - 0.5 * Ld;
					vec3 L1 = VectorToLight + 0.5 * Ld;

					// Shortest distance
					float a = Square( SourceLength );
					float b = dot( R, Ld );
					float t = clamp( dot( L0, b*R - Ld ) / (a - b*b), 0.0, 1.0 );

					VectorToLight = L0 + t * Ld;
			}
		   
			if( SourceRadius > 0.0 )
			{
					float SphereAngle = clamp( SourceRadius * RLengthL, 0.0, 1.0 );
					Energy *= Square( a / clamp( a + 0.5 * SphereAngle, 0.0, 1.0 ) );
				   
					// Closest point on sphere to ray
					vec3 ClosestPointOnRay = dot( VectorToLight, R ) * R;
					vec3 CenterToRay = ClosestPointOnRay - VectorToLight;
					vec3 ClosestPointOnSphere = VectorToLight + CenterToRay * clamp( SourceRadius * inversesqrt( dot( CenterToRay, CenterToRay ) ), 0.0, 1.0 );
					VectorToLight = ClosestPointOnSphere;
			}
								   
			// TODO: код Spot
		   
			//vec3 L =  VectorToLight;
			vec3 L = normalize( VectorToLight );

			vec3 H = normalize(V + L);
			float NoL = clamp( dot(N, L), 0.0, 1.0 );
			float NoV = clamp( dot(N, V), 0.0, 1.0 );
			float NoH = clamp( dot(N, H), 0.0, 1.0 );
			float VoH = clamp( dot(V, H), 0.0, 1.0 );                     
		   
			// Generalized microfacet specular
			float D = Distribution( roughness, NoH );
			float Vis = GeometricVisibility( roughness, NoV, NoL, VoH, L, V );
			vec3 F = Fresnel( specular, VoH );
			return (Energy * D * Vis) * F;
	}

	
	vec3 PointLightSubsurface(float alpha, vec3 L, vec3 V, vec3 N, float SubsurfaceExtinction )
	{
			vec3 _SubColor = vec3(1.0);
		
			vec3 H = normalize(V + L);
	
			float InScatter = pow(clamp(dot(L, -V), 0.0, 1.0), 12.0) * mix(3.0, 0.1, alpha);
		   
			float OpacityFactor = alpha;

			float NormalContribution = clamp(dot(N, H) * OpacityFactor + 1.0 - OpacityFactor, 0.0, 1.0);
			
			//float BackScatter = InGBufferData.GBufferAO * NormalContribution / (PI * 2.0);
			float BackScatter = NormalContribution / (PI * 2.0);
		   
			return  _SubColor * (mix(BackScatter, 1.0, InScatter) * SubsurfaceExtinction);
	}
	
	float LuminanceUE( vec3 LinearColor )
	{
		return dot( LinearColor, vec3( 0.3, 0.59, 0.11 ) );
	}
vec3 EnvBRDFApprox( vec3 SpecularColor, float Roughness, float NoV )
{

	const vec4 c0 = vec4(-1., -0.0275, -0.572, 0.022);
	const vec4 c1 = vec4(1.0, 0.0425, 1.04, -0.04 );
	vec4 r = Roughness * c0 + c1;
	float a004 = min( r.x * r.x, exp2( -9.28 * NoV ) ) * r.x + r.y;
	vec2 AB = vec2( -1.04, 1.04 ) * a004 + r.zw;


	AB.y *= clamp( 50.0 * SpecularColor.g, 0.0, 1.0 );


	return SpecularColor * AB.x + AB.y;
}
	
	vec4 GetDynamicLighting(vec3 specular, float _Roughness, vec3 albedo, float alpha, vec3 lightDir, vec3 viewDir, float atten,  vec3 normal, float ambientOcclusion)
	{
		//float3 DiffuseColor = BaseColor - BaseColor * Metallic;
	
	
		vec3 N = normalize( normal );                   
		vec3 L = normalize( lightDir ); 
		float NoL = BiasedNDotL( dot(N, L) );
		float DistanceAttenuation = atten;
		float LightRadiusMask = 1.0;
		float SpotFalloff = 1.0;

		vec4 OutLighting = vec4(0.0);
			   
		if (LightRadiusMask > 0.0 && SpotFalloff > 0.0)
		{
				float OpaqueShadowTerm = 1.0;
				float SSSShadowTerm = 1.0;
			   
				float NonShadowedAttenuation = DistanceAttenuation * LightRadiusMask * SpotFalloff;
				//float ShadowedAttenuation = NonShadowedAttenuation * OpaqueShadowTerm;
				
				//float term = dot(L, N) * 100000.0;
				
				float ShadowedAttenuation = NonShadowedAttenuation * OpaqueShadowTerm ;
				
				ShadowedAttenuation += ambientOcclusion;
				
						
				
				if (ShadowedAttenuation > 0.0)
				{
						vec3 V = normalize(viewDir);
						float NoV = max( dot( N, V ), 0.0 );
						
						vec3 DielectricSpecular = 0.08 * specular;
						vec3 metallicTerm = albedo * metallic;
						
						vec3 LightColor =  albedo - metallicTerm;
						vec3 SpecularColor = (DielectricSpecular - DielectricSpecular * metallic ) + metallicTerm;
						//SpecularColor = EnvBRDFApprox( SpecularColor, _Roughness, NoV );
						
		
	
						
						vec3 DiffuseLighting = PointLightDiffuse( _Roughness, albedo, L, V, N );
						vec3 SpecularLighting = PointLightSpecular( specular,  _Roughness, L, V, N );
						vec3 SubsurfaceLighting = PointLightSubsurface( 0.0, L, viewDir, N, SSSShadowTerm );   
					   
						vec4 LightColor4 = vec4(albedo, LuminanceUE(albedo));
						vec4 DiffuseLighting4 = vec4(DiffuseLighting, 0.0);
						
						
						
						
						vec4 SpecularLighting4 = vec4(SpecularLighting, LuminanceUE(SpecularLighting));
						vec4 SubsurfaceLighting4 = vec4(SubsurfaceLighting, 0.0);
					   
						OutLighting += LightColor4 * (  (NoL * ShadowedAttenuation) *
																(DiffuseLighting4 + SpecularLighting4) +
																SubsurfaceLighting4 * NonShadowedAttenuation );
						
						
						vec3 R = reflect(V, normalize(normal));
						vec3 reflection = textureCube(reflectionSampler, R).xyz;
										

						OutLighting += vec4(reflection * SpecularColor.rgb, 1.0) * vec4( max(0.0, 1.0 - (_Roughness * 2. ) ) );
						
						//OutLighting = SubsurfaceLighting;
				}
		}

		return OutLighting;
	}

		
	vec3 VSPositionFromDepth(vec2 vTexCoord, float depth)
	{

		float z = depth;  

		float x = vTexCoord.x * 2.0 - 1.0;
		float y = (1.0 - vTexCoord.y) * 2.0 - 1.0;
		vec4 projectedPos = vec4(x, y, z, 1.0);

		vec4 vPositionVS = projectedPos * InvProjection;  

		return vPositionVS.xyz / vPositionVS.w;  
	}
vec3 unpackColor(float f) 
{
    vec3 color;

    color.r = floor(f / 256.0 / 256.0);
    color.g = floor((f - color.r * 256.0 * 256.0) / 256.0);
    color.b = floor(f - color.r * 256.0 * 256.0 - color.g * 256.0);

    // now we have a vec3 with the 3 components in range [0..256]. Let's normalize it!
    return color / 256.0;
}
	

	void main() {
	
		vec4 depthNormal =  texture2D(normalSampler, v_uv);
		vec4 diffuseRoughness = texture2D(diffuseSampler, v_uv);
		float AmbientOcclusion = texture2D(ambientOcclusionSampler, v_uv).x;
		
		
		vec2 randomMap = texture2D(randomSampler, v_uv * 2.).xy / 2.0;
		
		vec3 normal = depthNormal.xyz;
		vec3 diffuse = diffuseRoughness.rgb;
		diffuse = diffuse;
		
		float roughness = diffuseRoughness.a;

		vec4 extraInfo = texture2D(infoSampler, v_uv);
		vec3 position = extraInfo.xyz;
		vec3 specular = vec3(extraInfo.w);

		//vec3 specular = vec3(0.1);

		vec3 ToLight = (lightPosition - position);
		vec3 viewDir = (cameraPosition - position);
		
		float alpha = 1.0;
		//float atten = properties_1.x;
		
		
		vec3 OutColor2 = GetDynamicLighting(specular, 
											roughness, 
											diffuse.rgb, 
											alpha, 
											ToLight, 
											viewDir, 
											attenuation,  
											normal,
											AmbientOcclusion ).xyz;
	
	
	
	
		//GetDynamicLighting(vec3 specular, float _Roughness, vec3 albedo, float alpha, vec3 lightDir, vec3 viewDir, float atten,  vec3 normal, float ambientOcclusion)
	
	//	if(v_uv.x < .5 && v_uv.y < .5) {
		
		//	gl_FragColor = vec4(diffuseRoughness.a);
			
		//} else {
		//	gl_FragColor = vec4(  vec3(shadingModelID / 20.), 1.0);
			//gl_FragColor = vec4(  PointLightSpecular( specular,  .4, ToLight, viewDir, normal ) * .1, 1.0);
		//	gl_FragColor = vec4( PointLightSubsurface( alpha, ToLight, viewDir, normal, 1.0 ), 1.0);
		//	gl_FragColor = vec4( PointLightDiffuse( .4, diffuse.rgb, ToLight, viewDir, normal ), 1.0);
			//gl_FragColor = vec4(normal, 1.0);
		//	gl_FragColor = vec4(diffuseRoughness.rgb, 1.0);
			
			

		
		
		
		gl_FragColor = vec4((OutColor2 ) , 1.0);//
			
			
			//gl_FragColor = vec4(normal, 1.0);
		//}
		
	}
	